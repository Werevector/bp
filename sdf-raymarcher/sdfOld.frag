#version 450

layout(location = 0) out vec4 color;
layout (location = 1) in vec2 f_texcoord;

// // const int width  = 640;
// // const int height = 480;
// const float width  = 640*2;
// const float height = 480*2;
// const ivec2 g_resolution = ivec2(width, height);

// //-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
// //                          SIGNED DISTANCE FIELDS                           |
// //  http://www.iquilezles.org/www/articles/distfunctions/distfunctions.htm   |
// //-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
float sdSphere(vec3 p, float r) {
    return length(p) - r;
}

// float udBox( vec3 p, vec3 b )
// {
//     return length(max(abs(p)-b,0.0));
// }

// float sdBox( vec3 p, vec3 b )
// {
//     vec3 d = abs(p) - b;
//     return min(max(d.x,max(d.y,d.z)),0.0) + length(max(d,0.0));
// }

// float sdPlane( vec3 p, vec4 n )
// {
//     return dot(p,n.xyz) + n.w;
// }

// float plane(vec3 p) {
//     return p.y;
// }

float sdTorus( vec3 p, vec2 t )
{
    vec2 q = vec2(length(p.xz)-t.x,p.y);
    return length(q)-t.y;
}

float sdOctahedron( in vec3 p, in float s)
{
    p = abs(p);
    float m = p.x+p.y+p.z-s;
    vec3 q;
         if( 3.0*p.x < m ) q = p.xyz;
    else if( 3.0*p.y < m ) q = p.yzx;
    else if( 3.0*p.z < m ) q = p.zxy;
    else return m*0.57735027;
    
    float k = clamp(0.5*(q.z-q.y+s),0.0,s); 
    return length(vec3(q.x,q.y-s+k,q.z-k)); 
}

// float opSmoothUnion( float d1, float d2, float k ) {
//     float h = clamp( 0.5 + 0.5*(d2-d1)/k, 0.0, 1.0 );
//     return mix( d2, d1, h ) - k*h*(1.0-h); 
// }

// //-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
// //            RAY CLASS            |
// //-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
// struct Ray
// {
//     vec3 origin;
//     vec3 direction;
// };

// vec3 ray_point(Ray r, float t) 
// {
//     return r.origin + r.direction * t;
// }

// //-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
// //          CAMERA CLASS           |
// //-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
// struct Camera
// {
//     vec3 origin;
//     vec3 lower_left_corner;
//     vec3 horizontal;
//     vec3 vertical;
//     vec3 u, v, w;
// };

// Camera initCamera(
//     vec3 lookfrom, 
//     vec3 lookat, 
//     vec3 vup, 
//     float vfov,
//     float aspect)
// {
//     float M_PI = 3.141592;
//     float theta = vfov*M_PI / 180.0;
//     float half_height = tan(theta / 2.0);
//     float half_width = aspect * half_height;

//     vec3 origin = lookfrom;
//     vec3 w = normalize(lookfrom - lookat); //look direction
//     vec3 u = normalize(cross(vup, w));
//     vec3 v = cross(w, u);
//     vec3 lower_left_corner = origin - half_width*u - half_height*v - w;
//     vec3 horizontal = 2.0 * half_width*u;
//     vec3 vertical = 2.0 * half_height*v;
//     return Camera(origin, lower_left_corner, horizontal, vertical, u, v, w);
// }

// Ray get_camera_ray(Camera c, float u, float v) 
// {
//     return Ray(c.origin, c.lower_left_corner + u*c.horizontal + v*c.vertical - c.origin);
// }

// //-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
// //             SCENE SDF           |
// //-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
// // float sceneDistance(vec3 p) {
// //     //float plane = sdPlane(point, normalize(vec4(0.0, 1.0, 0.0, 1.0)));
// //     //float displacement = sin(5.0 * p.x) * sin(5.0 * p.y) * sin(5.0 * p.z) * 0.25;
// //     //float d2 = sdOctahedron(p, 1);
// //     //float d3 = sdTorus(p, vec2(2,0.5));
    
// //     vec3 c = vec3(3, 0.0 ,3);
// //     vec3 q = mod(p, c) - 0.5 * c;

// //     float d1 = sdPlane(p, normalize(vec4(0.0, 1.0, 0.0, 1.0)));
// //     float n1 = sdSphere(p + vec3(0.0,-1.0,0.0), 1.0);
// //     //float d2 = opSmoothUnion( udBox(q - vec3(0.0, -0.5, 0.0), vec3(0.5, 0.5, 0.5)), n1, 1.0);
// //     float d2 = udBox(p - vec3(0.0, 1.0, 0.0), vec3(0.5,0.5,0.5));
// //     //float d2 = udBox(q, vec3(0.5,0.5,0.5));
// //     //float d3 = max(d2, );
    
// //     return min(d1, n1);
// //     //return d2;
// //     //return opSmoothUnion(d1, d2, 0.3);

// // }

// float sceneDistance( in vec3 pos )
// {
//     vec3 qos = vec3( fract(pos.x+0.5)-0.5, pos.yz );
//     return min(plane(pos.xyz-vec3(0.0, 0.0, 0.0)),
//                 sdBox(qos.xyz-vec3( 0.0,0.25, 0.0), vec3(0.2,0.5,0.2) ) );
// }

// // vec3 calculate_normal(in vec3 p)
// // {
// //     const vec3 small_step = vec3(0.0002, 0.0, 0.0);

// //     float gradient_x = sceneDistance(p + small_step.xyy) - sceneDistance(p - small_step.xyy);
// //     float gradient_y = sceneDistance(p + small_step.yxy) - sceneDistance(p - small_step.yxy);
// //     float gradient_z = sceneDistance(p + small_step.yyx) - sceneDistance(p - small_step.yyx);

// //     vec3 normal = vec3(gradient_x, gradient_y, gradient_z);

// //     return normalize(normal);
// // }

// vec3 calculate_normal( in vec3 p ) // for function f(p)
// {
//     const float h = 0.00001; // or some other value
//     const vec2 k = vec2(1,-1);
//     return normalize( k.xyy*sceneDistance( p + k.xyy*h ) + 
//                       k.yyx*sceneDistance( p + k.yyx*h ) + 
//                       k.yxy*sceneDistance( p + k.yxy*h ) + 
//                       k.xxx*sceneDistance( p + k.xxx*h ) );
// }

// float shadow( in vec3 ro, in vec3 rd, float mint, float maxt )
// {
//     for( float t=mint; t < maxt; )
//     {
//         float h = sceneDistance(ro + rd*t);
//         if( h<0.001 )
//             return 0.0;
//         t += h;
//     }
//     return 1.0;
// }

// // float softshadow( in vec3 ro, in vec3 rd, float mint, float maxt, float k )
// // {
// //     float res = 1.0;
// //     for( float t=mint; t < maxt; )
// //     {
// //         float h = sceneDistance(ro + rd*t);
// //         if( h<0.001 )
// //             return 0.0;
// //         res = min( res, k*h/t );
// //         t += h;
// //     }
// //     return res;
// // }

// float softshadow( in vec3 ro, in vec3 rd, float mint, float maxt, float k )
// {
//     float res = 1.0;
//     float ph = 1e20;
//     for( float t=mint; t < maxt; )
//     {
//         float h = sceneDistance(ro + rd*t);
//         if( h<0.001 )
//             return 0.0;
//         float y = h*h/(2.0*ph);
//         float d = sqrt(h*h-y*y);
//         res = min( res, k*d/max(0.0,t-y) );
//         ph = h;
//         t += h;
//     }
//     return res;
// }

// void main() {
//     float u = gl_FragCoord.x / g_resolution.x;
//     float v = gl_FragCoord.y / g_resolution.y;
//     float g_rmEpsilon = 0.0001;

//     // initial color is sky color
//     vec3 fcolor = vec3(1.0, 1.0, 1.0);

//     Camera camera = initCamera(
//         vec3(4, 4.0, -4.0),  //origin
//         vec3(0.0, -1.0, 0.0), //lookat
//         vec3(0.0, -1.0, 0.0),  //up
//         90.0,
//         g_resolution.x / g_resolution.y
//     );

//     float t = 0.0;
//     const int maxSteps = 64;
//     for(int i = 0; i < maxSteps; ++i)
//     {
//         float f = 1.0;

//         Ray ray = get_camera_ray(camera, u, v);
//         vec3 p = ray_point(ray, t);
//         float d = sceneDistance(p); // Distance to scene
    
//         if(d < g_rmEpsilon)
//         {
//             vec3 normal = calculate_normal(p);
//             vec3 light_position = vec3(5, -10.0, 0.1); //hard light
//             vec3 direction_to_light = normalize(p - light_position);
//             float diffuse_intensity = max (0.1, dot(normal, direction_to_light));
//             float shade = softshadow(p, direction_to_light, 0.1, 100.0, 52);
//             //fcolor = vec3(1.0/i, 0.0, 0.0) * diffuse_intensity;
//             //fcolor = vec3(1.0/i, 1.0/i, 1.0/i);
//             //fcolor = normal * diffuse_intensity;
//             //fcolor = vec3(1.0,1.0,1.0) * diffuse_intensity;
//             //fcolor = normal;
//             float o = 1.0-float(i)/float(maxSteps); 
//             //fcolor = vec3(o,o,o);
//             fcolor = vec3(o,o,o) * shade;
//             //fcolor = vec3(1,0,0);
//             break;
//         }   
//         t += d;
//     }
//     color = vec4(fcolor, 1.0);
// }

//    
// Testing Sebastian Aaltonen's soft shadow improvement
//
// The technique is based on estimating a better closest point in ray
// at each step by triangulating from the previous march step.
//
// More info about the technique at slide 39 of this presentation:
// https://www.dropbox.com/s/s9tzmyj0wqkymmz/Claybook_Simulation_Raytracing_GDC18.pptx?dl=0
//
// Traditional technique: http://iquilezles.org/www/articles/rmshadows/rmshadows.htm
//
// Go to lines 54 to compare both.


// make this 1 if your machine is too slow
#define AA 2

const float width  = 640*2;
const float height = 480*2;
const ivec2 iResolution = ivec2(width, height);

//------------------------------------------------------------------

float sdPlane( vec3 p )
{
	return p.y;
}

float sdBox( vec3 p, vec3 b )
{
    vec3 d = abs(p) - b;
    return min(max(d.x,max(d.y,d.z)),0.0) + length(max(d,0.0));
}


//------------------------------------------------------------------

float map( in vec3 pos )
{
    vec3 qos = vec3( fract(pos.x+0.5)-0.5, pos.yz );
    return min( 
        sdPlane(
            pos.xyz-vec3( 0.0,0.00, 0.0)),
            //sdBox(pos.xyz-vec3(0.0,0.25,0.0), vec3(0.2,0.5,0.2))
            max(sdOctahedron(qos.xyz-vec3(0.0,0.5,0.0),0.5), sdBox(qos.xyz-vec3(0.0,0.25,0.0), vec3(0.2,0.5,0.2)))
            //max(sdBox(pos.xyz-vec3(0.0,1.0,0.0),vec3(0.5,1.0,0.2)), -sdSphere(pos.xyz-vec3(0.0,0.5,0.0), 0.4))
            //sdBox(pos.xyz-vec3(0.0,1.0,0.0),vec3(1.0,1.0,1.0))
        );
}

//------------------------------------------------------------------

float calcSoftshadow( in vec3 ro, in vec3 rd, in float mint, in float tmax, int technique )
{
	float res = 1.0;
    float t = mint;
    float ph = 1e10; // big, such that y = 0 on the first iteration
    
    for( int i=0; i<32; i++ )
    {
		float h = map( ro + rd*t );

        // traditional technique
        if( technique==0 )
        {
        	res = min( res, 10.0*h/t );
        }
        // improved technique
        else
        {
            // use this if you are getting artifact on the first iteration, or unroll the
            // first iteration out of the loop
            //float y = (i==0) ? 0.0 : h*h/(2.0*ph); 

            float y = h*h/(2.0*ph);
            float d = sqrt(h*h-y*y);
            res = min( res, 10.0*d/max(0.0,t-y) );
            ph = h;
        }
        
        t += h;
        
        if( res<0.0001 || t>tmax ) break;
        
    }
    return clamp( res, 0.0, 1.0 );
}

vec3 calcNormal( in vec3 pos )
{
    vec2 e = vec2(1.0,-1.0)*0.5773*0.0005;
    return normalize( e.xyy*map( pos + e.xyy ) + 
					  e.yyx*map( pos + e.yyx ) + 
					  e.yxy*map( pos + e.yxy ) + 
					  e.xxx*map( pos + e.xxx ) );
}

float castRay( in vec3 ro, in vec3 rd )
{
    float tmin = 1.0;
    float tmax = 20.0;
   
#if 1
    // bounding volume
    float tp1 = (0.0-ro.y)/rd.y; if( tp1>0.0 ) tmax = min( tmax, tp1 );
    float tp2 = (1.0-ro.y)/rd.y; if( tp2>0.0 ) { if( ro.y>1.0 ) tmin = max( tmin, tp2 );
                                                 else           tmax = min( tmax, tp2 ); }
#endif
    
    float t = tmin;
    for( int i=0; i<64; i++ )
    {
	    float precis = 0.0005*t;
	    float res = map( ro+rd*t );
        if( res<precis || t>tmax ) break;
        t += res;
    }

    if( t>tmax ) t=-1.0;
    return t;
}

float calcAO( in vec3 pos, in vec3 nor )
{
	float occ = 0.0;
    float sca = 1.0;
    for( int i=0; i<5; i++ )
    {
        float h = 0.001 + 0.15*float(i)/4.0;
        float d = map( pos + h*nor );
        occ += (h-d)*sca;
        sca *= 0.95;
    }
    return clamp( 1.0 - 1.5*occ, 0.0, 1.0 );    
}

vec3 render( in vec3 ro, in vec3 rd, in int technique)
{ 
    vec3  col = vec3(0.0);
    float t = castRay(ro,rd);

    if( t>-0.5 )
    {
        vec3 pos = ro + t*rd;
        vec3 nor = calcNormal( pos );
        
        // material        
		vec3 mate = vec3(0.3);

        // key light
        vec3  lig = normalize( vec3(-0.1, 0.3, 0.6) );
        vec3  hal = normalize( lig-rd );
        float dif = clamp( dot( nor, lig ), 0.0, 1.0 ) * 
                    calcSoftshadow( pos, lig, 0.01, 300.0, technique );

		float spe = pow( clamp( dot( nor, hal ), 0.0, 1.0 ),16.0)*
                    dif *
                    (0.04 + 0.96*pow( clamp(1.0+dot(hal,rd),0.0,1.0), 5.0 ));

		col = mate * 4.0*dif*vec3(1.00,0.70,0.5);
        col +=      12.0*spe*vec3(1.00,0.70,0.5);
        
        // ambient light
        float occ = calcAO( pos, nor );
		float amb = clamp( 0.5+0.5*nor.y, 0.0, 1.0 );
        col += mate*amb*occ*vec3(0.0,0.08,0.1);
        
        // fog
        col *= exp( -0.0005*t*t*t );
    }

	return col;
}

mat3 setCamera( in vec3 ro, in vec3 ta, float cr )
{
	vec3 cw = normalize(ta-ro);
	vec3 cp = vec3(sin(cr), cos(cr),0.0);
	vec3 cu = normalize( cross(cw,cp) );
	vec3 cv = normalize( cross(cu,cw) );
    return mat3( cu, -cv, cw );
}

void main()
{
    // camera
    vec3 ro = vec3( 2.5, 2.5, -2.5);
    vec3 ta = vec3( 0.0, 0.5, 0.0 );
    
    // camera-to-world transformation
    mat3 ca = setCamera( ro, ta, 0.0 );

    vec3 tot = vec3(0.0);
#if AA>1
    for( int m=0; m<AA; m++ )
    for( int n=0; n<AA; n++ )
    {
        // pixel coordinates
        vec2 o = vec2(float(m),float(n)) / float(AA) - 0.5;
        vec2 p = (-iResolution.xy + 2.0*(gl_FragCoord.xy+o))/iResolution.y;
#else    
        vec2 p = (-iResolution.xy + 2.0*gl_FragCoord.xy)/iResolution.y;
#endif

        // ray direction
        vec3 rd = ca * normalize( vec3(p.xy,2.0) );

        // render	
        vec3 col = render( ro, rd, 1);

		// gamma
        col = pow( col, vec3(0.4545) );

        tot += col;
#if AA>1
    }
    tot /= float(AA*AA);
#endif

    color = vec4( tot, 1.0 );
}