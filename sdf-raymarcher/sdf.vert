#version 450

layout (location = 0) in vec2 v_pos;
layout (location = 1) out vec2 f_texcoord;

void main() {
    gl_Position = vec4(v_pos, 0.0, 1.0);
    f_texcoord = (v_pos + 1.0) / 2.0;
}