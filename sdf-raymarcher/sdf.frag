#version 450

layout(location = 0) out vec4 color;
layout (location = 1) in vec2 f_texcoord;

const float width  = 1000;
const float height = 1000;
const ivec2 g_resolution = ivec2(width, height);

// //-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
// //                          SIGNED DISTANCE FIELDS                           |
// //  http://www.iquilezles.org/www/articles/distfunctions/distfunctions.htm   |
// //-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
float sdSphere(vec3 p, float r) {
    return length(p) - r;
}

float udBox( vec3 p, vec3 b )
{
    return length(max(abs(p)-b,0.0));
}

float sdBox( vec3 p, vec3 b )
{
    vec3 d = abs(p) - b;
    return min(max(d.x,max(d.y,d.z)),0.0) + length(max(d,0.0));
}

float sdPlane( vec3 p, vec4 n )
{
    return dot(p,n.xyz) + n.w;
}

float yPlane(vec3 p) {
    return p.y;
}

float sdTorus( vec3 p, vec2 t )
{
    vec2 q = vec2(length(p.xz)-t.x,p.y);
    return length(q)-t.y;
}

float sdOctahedron( in vec3 p, in float s)
{
    p = abs(p);
    float m = p.x+p.y+p.z-s;
    vec3 q;
         if( 3.0*p.x < m ) q = p.xyz;
    else if( 3.0*p.y < m ) q = p.yzx;
    else if( 3.0*p.z < m ) q = p.zxy;
    else return m*0.57735027;
    
    float k = clamp(0.5*(q.z-q.y+s),0.0,s); 
    return length(vec3(q.x,q.y-s+k,q.z-k)); 
}

float opSmoothUnion( float d1, float d2, float k ) {
    float h = clamp( 0.5 + 0.5*(d2-d1)/k, 0.0, 1.0 );
    return mix( d2, d1, h ) - k*h*(1.0-h); 
}

float pylon(in vec3 p, in float s)
{
    float b = max(
        sdSphere(p, s*1.3),
        udBox(p, vec3(s))
    );
    return b;
}

//-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
//             SCENE SDF           |
//-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
float map( in vec3 pos )
{
    vec3 c = vec3(2, 0.0 ,2);
    vec3 qos = mod(pos, c) - 0.5 * c;
    vec3 pes = pos;
    return min( 
        yPlane(pos.xyz-vec3(0.0,0.0,0.0)),
        min(
            opSmoothUnion(
                sdBox(pes.xyz-vec3(0.0, 0.0, 0.0), vec3(1.0,0.5,1.0)),
                sdBox(pes.xyz-vec3(0.0, 0.25, 0.0), vec3(0.5, 0.5, 0.5)),
                0.1
            ),
            //sdOctahedron(pes.xyz-vec3(0.0, 1.1, 0.0), 0.3)
            pylon(pes.xyz-vec3(0.0, 1.2, 0.0), 0.25)
        )
    );
}

//-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
//            RAY CLASS            |
//-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
struct Ray
{
    vec3 origin;
    vec3 direction;
};

vec3 ray_point(Ray r, float t) 
{
    return r.origin + r.direction * t;
}

//-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
//          CAMERA CLASS           |
//-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
struct Camera
{
    vec3 ro;
    vec3 ta;
    mat3 cmatrix;
};

Camera initCamera(in vec3 ro, in vec3 ta, float cr)
{
    vec3 cw = normalize(ta-ro);
	vec3 cp = vec3(sin(cr), cos(cr),0.0);
	vec3 cu = normalize( cross(cw,cp) );
	vec3 cv = normalize( cross(cu,cw) );
    mat3 m = mat3( cu, -cv, cw );
    return Camera(ro, ta, m);
}

Ray cameraRay(in Camera c, in int m, in int n, in int AA)
{
    vec2 o = vec2(float(m),float(n)) / float(AA) - 0.5;
    vec2 p = (-g_resolution.xy + 2.0*(gl_FragCoord.xy+o)) / g_resolution.y;
    vec3 rd = c.cmatrix * normalize( vec3(p.xy,2.0) );
    return Ray(c.ro, rd);
}

vec3 calculate_normal( in vec3 p )
{
    const float h = 0.00001;
    const vec2 k = vec2(1,-1);
    return normalize( k.xyy*map( p + k.xyy*h ) + 
                      k.yyx*map( p + k.yyx*h ) + 
                      k.yxy*map( p + k.yxy*h ) + 
                      k.xxx*map( p + k.xxx*h ) );
}

float calcSoftshadow( in vec3 ro, in vec3 rd, in float mint, in float tmax)
{
	float res = 1.0;
    float t = mint;
    float ph = 1e10;
    
    for( int i=0; i<32; i++ )
    {
		float h = map( ro + rd*t );

        // use this if you are getting artifact on the first iteration, or unroll the
        // first iteration out of the loop
        float y = (i==0) ? 0.0 : h*h/(2.0*ph); 

        //float y = h*h/(2.0*ph);
        float d = sqrt(h*h-y*y);
        res = min( res, 10.0*d/max(0.0,t-y) );
        ph = h;
        
        t += h;
        
        if( res<0.0001 || t>tmax ) break;
        
    }
    return clamp( res, 0.0, 1.0 );
}

float castRay( in Ray ray )
{
    float tmin = 1.0;
    float tmax = 20.0;
    
    // bounding volume
    // float tp1 = (0.0-ray.origin.y)/ray.direction.y; 
    // if( tp1>0.0 ) tmax = min( tmax, tp1 );
    // float tp2 = (1.0-ray.origin.y)/ray.direction.y; 
    // if( tp2>0.0 ) 
    // { 
    //     if( ray.origin.y>1.0 ) tmin = max( tmin, tp2 );
    //     else tmax = min( tmax, tp2 ); 
    // }
    
    float t = tmin;
    for( int i=0; i<64; i++ )
    {
	    float precis = 0.0005*t;
	    float res = map( ray_point(ray, t) );
        if( res<precis || t>tmax ) break;
        t += res;
    }

    if( t>tmax ) t=-1.0;
    return t;
}

float calcAO( in vec3 pos, in vec3 nor )
{
	float occ = 0.0;
    float sca = 1.0;
    for( int i=0; i<5; i++ )
    {
        float h = 0.001 + 0.15*float(i)/4.0;
        float d = map( pos + h*nor );
        occ += (h-d)*sca;
        sca *= 0.95;
    }
    return clamp( 1.0 - 1.5*occ, 0.0, 1.0 );    
}

vec3 checkermat(in vec3 pos, in float freq) {
    float sines = sin(pos.x*freq) * sin(pos.y*freq) * sin(pos.z*freq);
    return sines < 0 ? vec3(1.0, 1.0, 1.0) : vec3(0.0,0.0,0.0);
}

vec3 linemat(in vec3 pos, in vec3 col, float threshold)
{
    if ( abs(pos.x) < threshold || abs(pos.z) < threshold) { return col; }
    else {return vec3(0.0, 0.0, 0.0);}
}

vec3 debugmat(in vec3 pos, in vec3 lcol, float threshold, float freq) 
{
    if ( pos.y < 0.01 ) { return vec3 (0.1); }
    if ( pos.y > 0.8 ) { return vec3 (0.0, 0.8, 0.5); }
    if ( abs(pos.x) < threshold || abs(pos.z) < threshold) { return lcol; }
    float sines = sin(pos.x*freq) * sin(pos.y*freq) * sin(pos.z*freq);
    return sines < 0 ? vec3(1.0, 1.0, 1.0) : vec3(0.0,0.0,0.0);
}

vec3 render( in Ray ray)
{ 
    vec3  col = vec3(0.0);
    float t = castRay(ray);

    if( t>-0.5 )
    {
        vec3 pos = ray.origin + t*ray.direction;
        vec3 nor = calculate_normal( pos );
        
        // material        
		vec3 mate = debugmat(pos, vec3(1.0,0.0,0.0), 0.02, 20);

        // key light
        vec3  lig = normalize( vec3(0.5, 0.3, 0.5) );
        vec3  hal = normalize( lig-ray.direction );
        float dif = clamp( dot( nor, lig ), 0.0, 1.0 ) * 
                    calcSoftshadow( pos, lig, 0.01, 300.0);

		float spe = pow( clamp( dot( nor, hal ), 0.0, 1.0 ),16.0)*
                    dif *
                    (0.04 + 0.96*pow( clamp(1.0+dot(hal,ray.direction),0.0,1.0), 5.0 ));

		col = mate * 4.0*dif*vec3(1.00,0.70,0.5);
        col +=      12.0*spe*vec3(1.00,0.70,0.5);
        
        // ambient light
        float occ = calcAO( pos, nor );
		float amb = clamp( 0.5+0.5*nor.y, 0.0, 1.0 );
        col += mate*amb*occ*vec3(0.0,0.08,0.1);
        
        // fog
        col *= exp( -0.0005*t*t*t );
    }

	return col;
}

void main()
{
    int AA = 2; 
    vec3 tot = vec3(0.0);

    Camera camera = initCamera(
        vec3(2.5, 2.5, -2.5),
        vec3(0.0, 0.0, 0.0),
        0.0
    );

    for( int m=0; m<AA; m++ )
    for( int n=0; n<AA; n++ )
    {
        Ray ray = cameraRay(camera, m, n, AA);
        vec3 col = render(ray);
        col = pow(col, vec3(0.4545) );
        tot += col;
    }
    tot /= float(AA*AA);
    color = vec4( tot, 1.0 );
}