#pragma once
#include <memory>
#include <future>

namespace bp {

class buffer;
class texture;
class render_pass;
class render_pass_instance;
class framebuffer;

class batch {
public:
	virtual ~batch() = default;

	virtual void upload(std::shared_ptr<bp::buffer> buffer) = 0;

	virtual void upload(std::shared_ptr<bp::texture> texture) = 0;

	virtual std::shared_ptr<render_pass_instance> render_pass(
		std::shared_ptr<bp::render_pass> pass,
		std::shared_ptr<framebuffer> target,
		int32_t x_pos,
		int32_t y_pos,
		uint32_t width,
		uint32_t height
	) = 0;

	virtual std::future<void> execute() = 0;
};

}