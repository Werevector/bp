#pragma once
#include <memory>
#include <cstdint>

namespace bp {

class attachment;
class swapchain;
class texture;

class framebuffer {
public:
	virtual ~framebuffer() = default;

	virtual std::size_t width() const = 0;

	virtual std::size_t height() const = 0;

	virtual void resize(std::size_t width, std::size_t height) = 0;

	virtual void bind(
		std::shared_ptr<bp::attachment> attachment,
		std::shared_ptr<bp::texture> texture
	) = 0;

	virtual void bind(
		std::shared_ptr<bp::attachment> attachment,
		std::shared_ptr<bp::swapchain> swapchain
	) = 0;
};

}