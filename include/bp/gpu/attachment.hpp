#pragma once

namespace bp {

class attachment_flags {
	int m_flags;
public:
	enum {
		clear = 0x0001,
		load = 0x0002,
		store = 0x0004
	};

	attachment_flags() : m_flags{0} {}
	attachment_flags(int flags) : m_flags{flags} {}
	operator int() const { return m_flags; }
	int operator=(int flags) { return (m_flags = flags); }
	int operator|=(int flags) { return (m_flags |= flags); }
};

class attachment {
public:
	virtual ~attachment() = default;

	virtual attachment_flags flags() const = 0;

	virtual void clear_color(float r, float g, float b, float a) = 0;
};

}