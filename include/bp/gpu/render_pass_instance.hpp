#pragma once
#include <memory>
#include <cstdint>

namespace bp {

class subpass;
class subpass_instance;

class render_pass_instance {
public:
	virtual ~render_pass_instance() = default;

	virtual std::shared_ptr<subpass_instance> subpass(
		std::shared_ptr<bp::subpass> pass
	) = 0;

	virtual void render_area(
		int32_t x_pos,
		int32_t y_pos,
		uint32_t width,
		uint32_t height
	) = 0;
};

}