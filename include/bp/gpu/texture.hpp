#pragma once
#include <cstdint>

namespace bp {

enum class texture_type {
	texture_2D,
	texture_3D
};

enum class texture_format {
	r8,
	r8g8,
	r8g8b8a8,
	depth
};

class texture_usage {
	int m_flags;
public:
	enum {
		attachment = 0x0001,
		sampled = 0x0002,
	};

	texture_usage() : m_flags{0} {}
	texture_usage(int flags) : m_flags{flags} {}
	operator int() const { return m_flags; }
	int operator=(int flags) { return (m_flags = flags); }
	int operator|=(int flags) { return (m_flags |= flags); }
};

class texture {
public:
	virtual ~texture() = default;

	virtual texture_type type() const = 0;

	virtual texture_format format() const = 0;

	virtual texture_usage usage() const = 0;

	virtual std::size_t width() const = 0;

	virtual std::size_t height() const = 0;

	virtual std::size_t depth() const = 0;

	virtual std::size_t size() const = 0;

	virtual void resize(
		std::size_t width,
		std::size_t height,
		std::size_t depth = 1
	) = 0;

	virtual std::uint8_t* map() = 0;

	virtual void unmap() = 0;
};

}