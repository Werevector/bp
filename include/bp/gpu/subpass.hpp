#pragma once
#include <memory>

namespace bp {

class attachment;
enum class pipeline_stage;

class subpass {
public:
	virtual ~subpass() = default;

	virtual void add_input_attachment(std::shared_ptr<attachment> input) = 0;

	virtual void add_color_attachment(
		std::shared_ptr<attachment> color,
		std::shared_ptr<attachment> resolve = std::shared_ptr<attachment>{}
	) = 0;

	virtual void set_depth_attachment(
		std::shared_ptr<attachment> depth
	) = 0;

	virtual void add_preserve_attachment(
		std::shared_ptr<attachment> preserve
	) = 0;

	virtual void add_dependency(
		std::shared_ptr<subpass> src,
		pipeline_stage src_stage,
		pipeline_stage dst_stage
	) = 0;
};

}