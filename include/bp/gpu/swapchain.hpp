#pragma once
#include <cstdint>

namespace bp {

class swapchain_flags {
	int m_flags;
public:
	enum {
		vsync = 0x0001
	};

	swapchain_flags() : m_flags{0} {}
	swapchain_flags(int flags) : m_flags{flags} {}
	operator int() const { return m_flags; }
	int operator=(int flags) { return (m_flags = flags); }
	int operator|=(int flags) { return (m_flags |= flags); }
};

class swapchain {
public:
	virtual ~swapchain() = default;

	virtual swapchain_flags flags() const = 0;

	virtual std::size_t width() const = 0;

	virtual std::size_t height() const = 0;

	virtual void resize(std::size_t width, std::size_t height) = 0;

	virtual void swap() = 0;
};

}