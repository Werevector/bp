#pragma once
#include <string>

namespace bp {

enum class shader_stage {
	vertex,
	tesselation_control,
	tesselation_evaluation,
	geometry,
	fragment,
	compute,
};

enum class shader_data_type {
	undefined,
	b,
	bvec2,
	bvec3,
	bvec4,
	i,
	ivec2,
	ivec3,
	ivec4,
	f,
	vec2,
	vec3,
	vec4,
	mat2,
	mat3,
	mat4
};

struct shader_io {
	shader_data_type type{shader_data_type::undefined};
	unsigned location{0};
};

class shader {
public:
	virtual ~shader() = default;

	virtual shader_stage stage() const = 0;

	virtual shader_io get_input(const std::string& name) const = 0;

	virtual shader_io get_output(const std::string& name) const = 0;
};

}