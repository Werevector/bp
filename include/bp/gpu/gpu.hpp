#pragma once
#include <string>

namespace bp {

/**
 * Bitmask of GPU usage flags.
 *
 * Example usage:
 * @code
 * bp::gpu_usage usage = bp::gpu_usage::graphics | bp::gpu_usage::surface;
 * @endcode
 */
class gpu_usage {
	int m_flags;
public:
	/**
	 * Enumeration of GPU usage flags.
	 *
	 * This enumeration defines the possible flags that specifies the desired
	 * usage of a GPU.
	 */
	enum gpu_usage_bits {
		graphics = 0x0001,	/**< Specifies graphics capability. */
		compute = 0x0002,	/**< Specifies compute capability. */
		surface = 0x0004	/**< Specifies surface capability. */
	};

	gpu_usage() : m_flags{0} {}
	gpu_usage(int flags) : m_flags{flags} {}
	operator int() const { return m_flags; }
	int operator=(int flags) { return (m_flags = flags); }
	int operator|=(int flags) { return (m_flags |= flags); }
};

/**
 * GPU representation.
 *
 * This class represents a specific GPU with an identifier and a name. It is
 * possible for two GPUs to have the same name.
 */
class gpu {
	unsigned m_id;
	std::string m_name;

public:
	gpu() : m_id{0} {}
	gpu(unsigned id, const std::string& name) :
		m_id{id}, m_name{name} {}

	unsigned id() const { return m_id; }
	const std::string& name() const { return m_name; }
};

}