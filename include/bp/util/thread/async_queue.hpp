#pragma once
#include <queue>
#include <mutex>
#include <condition_variable>

namespace bp {

template<typename T>
class async_queue
{
	std::queue<T> queue;
	std::mutex mutex;
	std::condition_variable enqueue_notifier;

public:
	void enqueue(T element)
	{
		std::lock_guard<std::mutex> lock{mutex};
		queue.push(element);
		enqueue_notifier.notify_one();
	}

	T dequeue()
	{
		std::unique_lock<std::mutex> lock{mutex};
		while (queue.empty()) enqueue_notifier.wait(lock);
		T element = queue.front();
		queue.pop();
		return element;
	}
};

}