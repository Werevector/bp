#pragma once
#include <GLFW/glfw3.h>
#include <memory>

namespace bp {

class instance;
class surface;

}

namespace bp_glfw {

std::shared_ptr<bp::instance> create_instance();

std::shared_ptr<bp::surface> create_surface(
	std::shared_ptr<bp::instance> instance,
	GLFWwindow* window
);

}