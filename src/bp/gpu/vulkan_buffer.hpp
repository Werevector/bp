#pragma once
#include "vk_mem_alloc.h"
#include <bp/gpu/buffer.hpp>
#include <memory>
#include <vulkan/vulkan.h>

#include <bp/util/api.hpp>

namespace bp {

class vulkan_context;

class BP_API vulkan_buffer : public buffer {
	std::shared_ptr<vulkan_context> context;
	buffer_usage m_usage;
	std::size_t m_size;
	VkBuffer m_handle;
	VmaMemoryUsage memory_usage;
	VmaAllocation memory_allocation;
	VkMemoryPropertyFlags memory_property_flags;
	std::unique_ptr<vulkan_buffer> staging_buffer;
	std::uint8_t* mapped;

public:
	vulkan_buffer(
		std::shared_ptr<vulkan_context> context,
		buffer_usage usage,
		std::size_t size,
		VmaMemoryUsage memory_usage
	);

	~vulkan_buffer();

	buffer_usage usage() const override { return m_usage; }

	std::size_t size() const override { return m_size; }

	std::uint8_t* map() override;

	void unmap() override;

	VkBuffer handle() { return m_handle; }

	VkBuffer staging_buffer_handle() {
		return staging_buffer ? staging_buffer->handle() : VK_NULL_HANDLE;
	}
};

}

#undef BP_API