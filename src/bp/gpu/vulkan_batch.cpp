#define BP_API_IMPL
#include "vulkan_batch.hpp"
#include "vulkan_context.hpp"
#include "vulkan_queue.hpp"
#include "vulkan_render_pass_instance.hpp"
#include "vulkan_render_pass.hpp"
#include "vulkan_framebuffer.hpp"
#include "vulkan_command_buffer.hpp"
#include "vulkan_semaphore.hpp"
#include "vulkan_swapchain.hpp"
#include "vulkan_buffer.hpp"
#include "vulkan_texture.hpp"
#include <bp/gpu/gpu_error.hpp>
#include <utility>

using namespace std;

namespace bp {

vulkan_batch::vulkan_batch(shared_ptr<vulkan_context> context) :
	context{context},
	transfer_cache{nullptr},
	graphics_cache{nullptr},
	compute_cache{nullptr} {
	cmd_buffer_cache.reserve(3);
	cmd_buffer_cache.emplace_back();
	auto& transfer_cache = cmd_buffer_cache.back();
	auto transfer_queue = context->transfer_queue();
	transfer_cache.queue = transfer_queue;
	transfer_cache.flags |= transfer;
	vulkan_batch::transfer_cache = &transfer_cache;

	auto graphics_queue = context->graphics_queue();
	if (graphics_queue) {
		if (graphics_queue == transfer_queue) {
			transfer_cache.flags |= graphics;
			graphics_cache = &transfer_cache;
		} else {
			cmd_buffer_cache.emplace_back();
			auto& graphics_cache = cmd_buffer_cache.back();
			graphics_cache.queue = graphics_queue;
			graphics_cache.flags |= graphics;
			vulkan_batch::graphics_cache = &graphics_cache;
		}
	}

	auto compute_queue = context->compute_queue();
	if (compute_queue) {
		if (compute_queue == transfer_queue) {
			transfer_cache.flags |= compute;
		} else if (compute_queue == graphics_queue) {
			cmd_buffer_cache.back().flags |= compute;
		} else {
			cmd_buffer_cache.emplace_back();
			auto& compute_cache = cmd_buffer_cache.back();
			compute_cache.queue = compute_queue;
			compute_cache.flags |= compute;
		}
	}

	unsigned fence_count = 1;
	if (graphics_queue) fence_count++;
	if (compute_queue) fence_count++;
	fences.resize(fence_count);

	VkFenceCreateInfo fence_info;
	fence_info.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;
	fence_info.pNext = nullptr;
	fence_info.flags = 0;

	for (unsigned i = 0; i < fence_count; i++) {
		VkResult result =
			vkCreateFence(context->device(), &fence_info, nullptr, &fences[i]);
		if (result != VK_SUCCESS) {
			for (unsigned j = 0; j < i; j++) {
				vkDestroyFence(context->device(), fences[j], nullptr);
			}
			throw gpu_error{
				"Failed to create fence for batch."
			};
		}
	}
}

vulkan_batch::~vulkan_batch() {
	for (auto f : fences) {
		vkDestroyFence(context->device(), f, nullptr);
	}
}

vulkan_batch::record_jobs vulkan_batch::prepare_transfer_commands(
	bp::vulkan_batch::queue_command_buffer_cache& cache
) {
	record_jobs jobs;

	cache.cmd_buffers.clear();
	cache.wait_semaphores.clear();
	cache.wait_dst_stage_masks.clear();
	cache.signal_semaphores.clear();

	if (!upload_buffers.empty()) {
		auto cmd_buf_job = cache.queue->record_commands(
			VK_COMMAND_BUFFER_LEVEL_PRIMARY,
			[this](VkCommandBuffer cmd_buf) {
				for (auto& b : upload_buffers) {
					VkBufferCopy copy_region = {};
					copy_region.srcOffset = 0;
					copy_region.dstOffset = 0;
					copy_region.size = b->size();
					vkCmdCopyBuffer(
						cmd_buf,
						b->staging_buffer_handle(),
						b->handle(),
						1,
						&copy_region);
				}
			}
		);
		jobs.push_back(move(cmd_buf_job));
	}

	if (!upload_textures.empty()) {
		auto cmd_buf_job = cache.queue->record_commands(
			VK_COMMAND_BUFFER_LEVEL_PRIMARY,
			[this](VkCommandBuffer cmd_buf) {
				vector<VkImageMemoryBarrier> barriers;
				for (auto& t : upload_textures) {
					barriers.push_back(t->transition(
						VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
						VK_ACCESS_TRANSFER_WRITE_BIT
					));
				}
				vkCmdPipelineBarrier(
					cmd_buf,
					VK_PIPELINE_STAGE_ALL_COMMANDS_BIT,
					VK_PIPELINE_STAGE_TRANSFER_BIT,
					0,
					0, nullptr,
					0, nullptr,
					static_cast<uint32_t>(barriers.size()), barriers.data()
				);

				for (auto& t : upload_textures) {
					VkImageSubresourceLayers sub_resource = {};
					sub_resource.aspectMask =
						t->format() == bp::texture_format::depth
						? VK_IMAGE_ASPECT_DEPTH_BIT
						: VK_IMAGE_ASPECT_COLOR_BIT;
					sub_resource.baseArrayLayer = 0;
					sub_resource.mipLevel = 0;
					//TODO 3D textures?
					sub_resource.layerCount = 1;

					VkBufferImageCopy region = {};
					region.imageSubresource = sub_resource;
					region.imageExtent = {
						static_cast<uint32_t>(t->width()),
						static_cast<uint32_t>(t->height()),
						1
					};

					vkCmdCopyBufferToImage(
						cmd_buf,
						t->staging_buffer_handle(),
						t->image_handle(),
						VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
						1, &region
					);
				}
			}
		);
		jobs.push_back(move(cmd_buf_job));
	}

	return jobs;
}

vulkan_batch::record_jobs vulkan_batch::prepare_graphics_commands(
	bp::vulkan_batch::queue_command_buffer_cache& cache
) {
	record_jobs jobs;

	cache.cmd_buffers.clear();
	cache.wait_semaphores.clear();
	cache.wait_dst_stage_masks.clear();
	cache.signal_semaphores.clear();

	for (auto& r : render_pass_instances) {
		jobs.push_back(
			r->prepare(
				cache.queue,
				[&cache](
					shared_ptr<vulkan_semaphore> wait_sem,
					VkPipelineStageFlags dst_stage_mask
				) {
					cache.wait_semaphores.push_back(wait_sem);
					cache.wait_dst_stage_masks.push_back(dst_stage_mask);
				},
				[&cache](shared_ptr<vulkan_semaphore> signal_sem) {
					cache.signal_semaphores.push_back(signal_sem);
				}
			)
		);
	}

	return jobs;
}

void vulkan_batch::upload(shared_ptr<bp::buffer> buffer) {
	auto vk_buffer = static_pointer_cast<vulkan_buffer>(buffer);
	if (vk_buffer->staging_buffer_handle() != VK_NULL_HANDLE) {
		upload_buffers.push_back(vk_buffer);
	}
}

void vulkan_batch::upload(shared_ptr<bp::texture> texture) {
	auto vk_texture = static_pointer_cast<vulkan_texture>(texture);
	if (vk_texture->staging_buffer_handle() != VK_NULL_HANDLE) {
		upload_textures.push_back(vk_texture);
	}
}

shared_ptr<render_pass_instance> vulkan_batch::render_pass(
	shared_ptr<bp::render_pass> pass,
	shared_ptr<framebuffer> framebuffer,
	int32_t x_pos,
	int32_t y_pos,
	uint32_t width,
	uint32_t height
) {
	if (!graphics_cache) {
		throw gpu_error{
			"Context does not support graphics."
		};
	}
	auto render_pass_instance = make_shared<vulkan_render_pass_instance>(
		shared_from_this(),
		static_pointer_cast<vulkan_render_pass>(pass),
		static_pointer_cast<vulkan_framebuffer>(framebuffer),
		x_pos,
		y_pos,
		width,
		height
	);
	render_pass_instances.push_back(render_pass_instance);
	connect(
		render_pass_instance->dirty_event,
		[cache = graphics_cache] {
			cache->flags |= dirty;
		}
	);
	return render_pass_instance;
}

future<void> vulkan_batch::execute() {
	record_jobs transfer_jobs;
	if (transfer_cache->flags & dirty) {
		transfer_jobs = prepare_transfer_commands(*transfer_cache);
	}

	record_jobs graphics_jobs;
	if (graphics_cache && graphics_cache->flags & dirty) {
		graphics_jobs = prepare_graphics_commands(*graphics_cache);
	}

	for (auto& job : transfer_jobs) {
		transfer_cache->cmd_buffers.push_back(job.get());
	}
	for (auto& job : graphics_jobs) {
		graphics_cache->cmd_buffers.push_back(job.get());
	}

	vector<VkFence> wait_fences;
	for (unsigned i = 0; i < cmd_buffer_cache.size(); i++) {
		auto& cache = cmd_buffer_cache[i];
		cache.flags &= ~dirty;

		if (cache.cmd_buffers.empty()) continue;

		vector<VkSemaphore> wait_sems;
		for (auto& s : cache.wait_semaphores) {
			wait_sems.push_back(s->handle());
		}

		vector<VkCommandBuffer> cmd_bufs;
		for (auto& c : cache.cmd_buffers) {
			cmd_bufs.push_back(c->handle());
		}

		vector<VkSemaphore> signal_sems;
		for (auto& s : cache.signal_semaphores) {
			signal_sems.push_back(s->handle());
		}

		VkSubmitInfo submit_info;
		submit_info.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
		submit_info.pNext = nullptr;
		submit_info.waitSemaphoreCount =
			static_cast<uint32_t>(wait_sems.size());
		submit_info.pWaitSemaphores = wait_sems.data();
		submit_info.pWaitDstStageMask = cache.wait_dst_stage_masks.data();
		submit_info.commandBufferCount = static_cast<uint32_t>(cmd_bufs.size());
		submit_info.pCommandBuffers = cmd_bufs.data();
		submit_info.signalSemaphoreCount =
			static_cast<uint32_t>(signal_sems.size());
		submit_info.pSignalSemaphores = signal_sems.data();

		VkResult result = vkQueueSubmit(
			cache.queue->handle(),
			1, &submit_info,
			fences[i]
		);
		if (result != VK_SUCCESS) {
			throw gpu_error{
				"Failed to submit batch."
			};
		}
		wait_fences.push_back(fences[i]);
	}

	VkDevice device = context->device();
	return async(launch::deferred, [wait_fences, device]{
		if (wait_fences.empty()) return;

		VkResult result = vkWaitForFences(
			device,
			static_cast<uint32_t>(wait_fences.size()),
			wait_fences.data(),
			VK_TRUE,
			UINT64_MAX
		);
		if (result != VK_SUCCESS) {
			throw gpu_error{
				"Failed to wait for batch completion."
			};
		}
		vkResetFences(
			device,
			static_cast<uint32_t>(wait_fences.size()),
			wait_fences.data()
		);
	});
}

}