#define BP_API_IMPL
#include "vulkan_command_pool.hpp"
#include "vulkan_command_buffer.hpp"
#include <bp/gpu/gpu_error.hpp>

using namespace std;

namespace bp {

void vulkan_command_pool::free(VkCommandBuffer cmd_buffer) {
	unique_lock pool_lk{m_mutex};
	remove_list.push_back(cmd_buffer);

	if (remove_list.size() >= 16) {
		vkFreeCommandBuffers(
			device,
			handle,
			static_cast<uint32_t>(remove_list.size()),
			remove_list.data()
		);
		remove_list.clear();
	}
}

vulkan_command_pool::vulkan_command_pool(
	VkDevice device,
	uint32_t family_index
) : device{device} {
	VkCommandPoolCreateInfo info;
	info.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
	info.pNext = nullptr;
	info.flags = 0;
	info.queueFamilyIndex = family_index;

	VkResult result = vkCreateCommandPool(device, &info, nullptr, &handle);
	if (result != VK_SUCCESS) {
		throw gpu_error{
			"Failed to create command pool."
		};
	}
}

vulkan_command_pool::~vulkan_command_pool() {
	vkDestroyCommandPool(device, handle, nullptr);
}

shared_ptr<vulkan_command_buffer>
vulkan_command_pool::allocate_command_buffer(
	VkCommandBufferLevel level
) {
	VkCommandBufferAllocateInfo info;
	info.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
	info.pNext = nullptr;
	info.commandPool = handle;
	info.level = level;
	info.commandBufferCount = 1;

	VkCommandBuffer cmd_buf;
	{
		unique_lock lk{m_mutex};
		VkResult result = vkAllocateCommandBuffers(device, &info, &cmd_buf);
		if (result != VK_SUCCESS) {
			throw gpu_error{
				"Failed to allocate command buffer."
			};
		}
	}

	return make_shared<vulkan_command_buffer>(
		cmd_buf,
		[this](VkCommandBuffer b) { free(b); }
	);
}

}