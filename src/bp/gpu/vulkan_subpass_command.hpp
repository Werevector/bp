#pragma once
#include <vulkan/vulkan.h>
#include <memory>

#include <bp/util/api.hpp>

namespace bp {

class vulkan_graphics_pipeline;

struct BP_API vulkan_subpass_command {
	vulkan_subpass_command() : type{none} {}

	enum {
		none,
		pipeline,
		vertex_buffers,
		index_buffer,
		draw,
		draw_indexed
	};
	int type;

	union {
		size_t pipeline_index;
		size_t vertex_buffers_index;
		size_t index_buffer_index;

		struct {
			uint32_t vertex_count;
			uint32_t instance_count;
			uint32_t first_vertex;
			uint32_t first_instance;
		} draw_data;

		struct {
			uint32_t index_count;
			uint32_t instance_count;
			uint32_t first_index;
			int32_t vertex_offset;
			uint32_t first_instance;
		} draw_indexed_data;
	};
};

}

#undef BP_API