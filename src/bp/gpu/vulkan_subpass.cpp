#define BP_API_IMPL
#include "vulkan_subpass.hpp"
#include "vulkan_attachment.hpp"
#include <bp/gpu/pipeline.hpp>
#include <bp/gpu/gpu_error.hpp>

using namespace std;

namespace bp {

void vulkan_subpass::add_input_attachment(shared_ptr<attachment> input) {
	auto vk_input = static_pointer_cast<vulkan_attachment>(input);
	vk_input->m_flags |= vulkan_attachment::input_attachment;
	if (render_pass.lock() != vk_input->render_pass.lock()) {
		throw gpu_error{
			"Given attachment is created from a different render pass"
		};
	}
	input_attachments.push_back(vk_input);
}

void vulkan_subpass::add_color_attachment(
	shared_ptr<attachment> color,
	shared_ptr<attachment> resolve
) {
	auto vk_color = static_pointer_cast<vulkan_attachment>(color);
	vk_color->m_flags |= vulkan_attachment::color_attachment;
	auto vk_resolve = static_pointer_cast<vulkan_attachment>(resolve);
	if (vk_resolve) {
		vk_resolve->m_flags |= vulkan_attachment::color_attachment;
	}

	auto pass = render_pass.lock();
	if (pass != vk_color->render_pass.lock()) {
		throw gpu_error{
			"Given color attachment is created from a different render pass"
		};
	}

	if (vk_resolve && pass != vk_resolve->render_pass.lock()) {
		throw gpu_error{
			"Given resolve attachment is created from a different render pass"
		};
	}

	color_attachments.push_back(vk_color);
	if (resolve_attachments.empty() && resolve) {
		for (auto i = 0; i < resolve_attachments.size(); i++) {
			resolve_attachments.emplace_back();
		}
		resolve_attachments.back() = vk_resolve;
	} else if (!resolve_attachments.empty()) {
		resolve_attachments.push_back(vk_resolve);
	}
}

void vulkan_subpass::set_depth_attachment(shared_ptr<attachment> depth) {
	auto vk_depth = static_pointer_cast<vulkan_attachment>(depth);
	vk_depth->m_flags |= vulkan_attachment::depth_attachment;
	if (render_pass.lock() != vk_depth->render_pass.lock()) {
		throw gpu_error{
			"Given attachment is created from a different render pass"
		};
	}
	depth_attachment = vk_depth;
}

void vulkan_subpass::add_preserve_attachment(shared_ptr<attachment> preserve) {
	auto vk_preserve = static_pointer_cast<vulkan_attachment>(preserve);
	if (render_pass.lock() != vk_preserve->render_pass.lock()) {
		throw gpu_error{
			"Given attachment is created from a different render pass"
		};
	}
	preserve_attachments.push_back(vk_preserve);
}

static VkPipelineStageFlags vk_pipeline_stage(pipeline_stage stage) {
	VkPipelineStageFlags s;
	switch (stage) {
	case pipeline_stage::top:
		s = VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT;
		break;
	case pipeline_stage::vertex_input:
		s = VK_PIPELINE_STAGE_VERTEX_INPUT_BIT;
		break;
	case pipeline_stage::vertex_shader:
		s = VK_PIPELINE_STAGE_VERTEX_SHADER_BIT;
		break;
	case pipeline_stage::tesselation_control_shader:
		s = VK_PIPELINE_STAGE_TESSELLATION_CONTROL_SHADER_BIT;
		break;
	case pipeline_stage::tesselation_evaluation_shader:
		s = VK_PIPELINE_STAGE_TESSELLATION_EVALUATION_SHADER_BIT;
		break;
	case pipeline_stage::geometry_shader:
		s = VK_PIPELINE_STAGE_GEOMETRY_SHADER_BIT;
		break;
	case pipeline_stage::fragment_shader:
		s = VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT;
		break;
	case pipeline_stage::early_fragment_tests:
		s = VK_PIPELINE_STAGE_EARLY_FRAGMENT_TESTS_BIT;
		break;
	case pipeline_stage::late_fragment_tests:
		s = VK_PIPELINE_STAGE_LATE_FRAGMENT_TESTS_BIT;
		break;
	case pipeline_stage::color_attachment_output:
		s = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
		break;
	}
	return s;
}

void vulkan_subpass::add_dependency(
	shared_ptr<subpass> src,
	pipeline_stage src_stage,
	pipeline_stage dst_stage
) {
	state &= ~source_subpass;
	dependencies.emplace_back();
	auto& d = dependencies.back();
	d.src = static_pointer_cast<vulkan_subpass>(src);
	d.src_stage = vk_pipeline_stage(src_stage);
	d.dst_stage = vk_pipeline_stage(dst_stage);
}

}