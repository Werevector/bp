#define BP_API_IMPL
#include "vulkan_shader.hpp"
#include "vulkan_context.hpp"
#include <bp/gpu/gpu_error.hpp>
#include <spirv_cross.hpp>

using namespace std;

namespace bp {

static shader_data_type shader_io_type(const spirv_cross::SPIRType& type) {
	shader_data_type dt = shader_data_type::undefined;

	switch (type.basetype) {
	case spirv_cross::SPIRType::Boolean:
		switch (type.vecsize) {
		case 1:
			dt = shader_data_type::b;
			break;
		case 2:
			dt = shader_data_type::bvec2;
			break;
		case 3:
			dt = shader_data_type::bvec3;
			break;
		case 4:
			dt = shader_data_type::bvec4;
			break;
		}
		break;
	case spirv_cross::SPIRType::Int:
		switch (type.vecsize) {
		case 1:
			dt = shader_data_type::i;
			break;
		case 2:
			dt = shader_data_type::ivec2;
			break;
		case 3:
			dt = shader_data_type::ivec3;
			break;
		case 4:
			dt = shader_data_type::ivec4;
			break;
		}
		break;
	case spirv_cross::SPIRType::Float:
		switch (type.vecsize) {
		case 1:
			dt = shader_data_type::f;
			break;
		case 2:
			dt = shader_data_type::vec2;
			break;
		case 3:
			dt = shader_data_type::vec3;
			break;
		case 4:
			dt = shader_data_type::vec4;
			break;
		}
		break;
	}

	return dt;
}

vulkan_shader::vulkan_shader(
	shared_ptr<bp::vulkan_context> context,
	bp::shader_stage stage,
	size_t spirv_code_size,
	const uint8_t* spirv_code
) : context{context} {
	m_stage_info.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
	m_stage_info.pNext = nullptr;
	m_stage_info.flags = 0;

	switch (stage) {
	case shader_stage::vertex:
		m_stage_info.stage = VK_SHADER_STAGE_VERTEX_BIT;
		break;
	case shader_stage::tesselation_control:
		m_stage_info.stage = VK_SHADER_STAGE_TESSELLATION_CONTROL_BIT;
		break;
	case shader_stage::tesselation_evaluation:
		m_stage_info.stage = VK_SHADER_STAGE_TESSELLATION_EVALUATION_BIT;
		break;
	case shader_stage::geometry:
		m_stage_info.stage = VK_SHADER_STAGE_GEOMETRY_BIT;
		break;
	case shader_stage::fragment:
		m_stage_info.stage = VK_SHADER_STAGE_FRAGMENT_BIT;
		break;
	case shader_stage::compute:
		m_stage_info.stage = VK_SHADER_STAGE_COMPUTE_BIT;
		break;
	}

	m_stage_info.module = VK_NULL_HANDLE;
	m_stage_info.pName = "main";
	m_stage_info.pSpecializationInfo = nullptr;

	VkShaderModuleCreateInfo info;
	info.sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;
	info.pNext = nullptr;
	info.flags = 0;
	info.codeSize = spirv_code_size;
	info.pCode = reinterpret_cast<const uint32_t*>(spirv_code);

	VkResult result = vkCreateShaderModule(
		context->device(),
		&info,
		nullptr,
		&m_stage_info.module
	);

	if (result != VK_SUCCESS)
	{
		throw gpu_error{
			"Failed to create shader module."
		};
	}

	spirv_cross::Compiler compiler{
		reinterpret_cast<const uint32_t*>(spirv_code),
		spirv_code_size / sizeof(uint32_t)
	};

	auto resources = compiler.get_shader_resources();

	for (auto& r : resources.stage_inputs) {
		string name = r.name;
		if (name.empty()) {
			name = compiler.get_fallback_name(r.id);
		}
		auto& in = inputs[name];
		in.type = shader_io_type(compiler.get_type(r.base_type_id));
		in.location = compiler.get_decoration(r.id, spv::DecorationLocation);
	}

	for (auto& r : resources.stage_outputs) {
		string name = r.name;
		if (name.empty()) {
			name = compiler.get_fallback_name(r.id);
		}
		auto& out = outputs[name];
		out.type = shader_io_type(compiler.get_type(r.base_type_id));
		out.location = compiler.get_decoration(r.id, spv::DecorationLocation);
	}
}

vulkan_shader::~vulkan_shader() {
	vkDestroyShaderModule(context->device(), handle(), nullptr);
}

shader_io vulkan_shader::get_input(const string& name) const {
	auto it = inputs.find(name);
	if (it == inputs.end()) return shader_io{};
	return it->second;
}

shader_io vulkan_shader::get_output(const string& name) const {
	auto it = outputs.find(name);
	if (it == outputs.end()) return shader_io{};
	return it->second;
}

}