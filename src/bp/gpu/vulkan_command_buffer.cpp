#define BP_API_IMPL
#include "vulkan_command_buffer.hpp"

using namespace std;

namespace bp {

vulkan_command_buffer::vulkan_command_buffer(
	VkCommandBuffer handle,
	function<void(VkCommandBuffer)> deleter
) : m_handle{handle},
	deleter{deleter} {}

vulkan_command_buffer::~vulkan_command_buffer() {
	deleter(m_handle);
}

}