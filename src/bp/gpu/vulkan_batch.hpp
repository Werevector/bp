#pragma once
#include <bp/gpu/batch.hpp>
#include <memory>
#include <vector>
#include <vulkan/vulkan.h>

#include <bp/util/api.hpp>

namespace bp {

class vulkan_context;
class vulkan_buffer;
class vulkan_texture;
class vulkan_queue;
class vulkan_command_buffer;
class vulkan_semaphore;
class vulkan_render_pass_instance;

class BP_API vulkan_batch :
	public batch,
	public std::enable_shared_from_this<vulkan_batch> {
	enum {
		transfer = 0x01,
		graphics = 0x02,
		compute = 0x04,
		dirty = 0x08
	};

	struct queue_command_buffer_cache {
		std::shared_ptr<vulkan_queue> queue;
		int flags{dirty};
		std::vector<std::shared_ptr<vulkan_command_buffer>> cmd_buffers;
		std::vector<std::shared_ptr<vulkan_semaphore>> wait_semaphores;
		std::vector<VkPipelineStageFlags> wait_dst_stage_masks;
		std::vector<std::shared_ptr<vulkan_semaphore>> signal_semaphores;
	};

	std::shared_ptr<vulkan_context> context;
	std::vector<std::shared_ptr<vulkan_buffer>> upload_buffers;
	std::vector<std::shared_ptr<vulkan_texture>> upload_textures;
	std::vector<std::shared_ptr<vulkan_render_pass_instance>>
		render_pass_instances;
	std::vector<queue_command_buffer_cache> cmd_buffer_cache;
	std::vector<VkFence> fences;

	queue_command_buffer_cache* transfer_cache;
	queue_command_buffer_cache* graphics_cache;
	queue_command_buffer_cache* compute_cache;

	using record_jobs =
		std::vector<std::future<std::shared_ptr<vulkan_command_buffer>>>;

	record_jobs prepare_transfer_commands(queue_command_buffer_cache& cache);
	record_jobs prepare_graphics_commands(queue_command_buffer_cache& cache);
public:
	vulkan_batch(std::shared_ptr<vulkan_context> context);

	~vulkan_batch();

	void upload(std::shared_ptr<bp::buffer> buffer) override;

	void upload(std::shared_ptr<bp::texture> texture) override;

	std::shared_ptr<render_pass_instance> render_pass(
		std::shared_ptr<bp::render_pass> pass,
		std::shared_ptr<framebuffer> framebuffer,
		int32_t x_pos,
		int32_t y_pos,
		uint32_t width,
		uint32_t height
	) override;

	std::future<void> execute() override;
};

}

#undef BP_API