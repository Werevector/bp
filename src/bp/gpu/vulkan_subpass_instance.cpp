#define BP_API_IMPL
#include "vulkan_subpass_instance.hpp"
#include "vulkan_subpass.hpp"
#include "vulkan_queue.hpp"
#include "vulkan_render_pass.hpp"
#include "vulkan_render_pass_instance.hpp"
#include "vulkan_framebuffer.hpp"
#include "vulkan_graphics_pipeline.hpp"
#include "vulkan_buffer.hpp"
#include "vulkan_subpass_command.hpp"
#include <bp/gpu/gpu_error.hpp>

using namespace std;

namespace bp {

vulkan_subpass_instance::vulkan_subpass_instance(
	shared_ptr<bp::vulkan_render_pass_instance> render_pass_instance,
	shared_ptr<bp::vulkan_subpass> subpass
) : render_pass_instance{render_pass_instance},
	subpass{subpass} {
	inheritance_info.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_INHERITANCE_INFO;
	inheritance_info.pNext = nullptr;
	inheritance_info.occlusionQueryEnable = VK_FALSE;
	inheritance_info.queryFlags = 0;
	inheritance_info.pipelineStatistics = 0;
}

future<shared_ptr<vulkan_command_buffer>> vulkan_subpass_instance::prepare(
	shared_ptr<bp::vulkan_queue> queue,
	VkViewport viewport,
	VkRect2D scissor
) {
	auto render_pass = subpass->render_pass.lock();
	auto instance = render_pass_instance.lock();
	inheritance_info.renderPass = render_pass->handle();
	inheritance_info.subpass = subpass->index;
	inheritance_info.framebuffer = instance->framebuffer()->current_handle();

	return queue->record_commands(
		VK_COMMAND_BUFFER_LEVEL_SECONDARY,
		[this, viewport, scissor](VkCommandBuffer cmd_buf){
			for (auto& cmd : commands) {
				switch (cmd.type) {
				case vulkan_subpass_command::pipeline:
					record_bind_pipeline(cmd_buf, cmd, viewport, scissor);
					break;
				case vulkan_subpass_command::vertex_buffers:
					record_bind_vertex_buffers(cmd_buf, cmd);
					break;
				case vulkan_subpass_command::index_buffer:
					record_bind_index_buffer(cmd_buf, cmd);
					break;
				case vulkan_subpass_command::draw:
					record_draw(cmd_buf, cmd);
					break;
				case vulkan_subpass_command::draw_indexed:
					record_draw_indexed(cmd_buf, cmd);
					break;
				}
			}
		},
		VK_COMMAND_BUFFER_USAGE_RENDER_PASS_CONTINUE_BIT,
		&inheritance_info
	);
}

void vulkan_subpass_instance::bind_pipeline(
	shared_ptr<bp::pipeline> pipeline
) {
	size_t index = pipelines.size();
	pipelines.push_back(
		static_pointer_cast<vulkan_graphics_pipeline>(pipeline)
	);
	commands.emplace_back();
	auto& cmd = commands.back();
	cmd.type = vulkan_subpass_command::pipeline;
	cmd.pipeline_index = index;
}

void vulkan_subpass_instance::push_constants(
	uint32_t offset,
	uint32_t size,
	const void* data
) {
	throw gpu_error{
		"Not implemented."
	};
}

void vulkan_subpass_instance::bind_vertex_buffers(
	vector<vertex_buffer_binding> bindings,
	uint32_t first_binding
) {
	size_t index = vertex_buffer_bindings.size();
	vertex_buffer_bindings.emplace_back();
	auto& vk_bindings = vertex_buffer_bindings.back();
	for (auto& b : bindings) {
		vk_bindings.buffers.push_back(
			static_pointer_cast<vulkan_buffer>(b.buffer)
		);
		vk_bindings.offsets.push_back(b.offset);
	}
	vk_bindings.first_binding = first_binding;

	commands.emplace_back();
	auto& cmd = commands.back();
	cmd.type = vulkan_subpass_command::vertex_buffers;
	cmd.vertex_buffers_index = index;
}

void vulkan_subpass_instance::bind_index_buffer(
	shared_ptr<bp::buffer> buffer,
	size_t offset,
	bp::index_type index_type
) {
	size_t index = index_buffer_bindings.size();
	index_buffer_bindings.emplace_back();
	auto& binding = index_buffer_bindings.back();
	binding.buffer = static_pointer_cast<vulkan_buffer>(buffer);
	binding.offset = offset;
	binding.index_type = index_type;

	commands.emplace_back();
	auto& cmd = commands.back();
	cmd.type = vulkan_subpass_command::index_buffer;
	cmd.index_buffer_index = index;
}

void vulkan_subpass_instance::draw(
	uint32_t vertex_count,
	uint32_t instance_count,
	uint32_t first_vertex,
	uint32_t first_instance
) {
	commands.emplace_back();
	auto& cmd = commands.back();
	cmd.type = vulkan_subpass_command::draw;
	cmd.draw_data.vertex_count = vertex_count;
	cmd.draw_data.instance_count = instance_count;
	cmd.draw_data.first_vertex = first_vertex;
	cmd.draw_data.first_instance = first_instance;
}

void vulkan_subpass_instance::draw_indexed(
	uint32_t index_count,
	uint32_t instance_count,
	uint32_t first_index,
	int32_t vertex_offset,
	uint32_t first_instance
) {
	commands.emplace_back();
	auto& cmd = commands.back();
	cmd.type = vulkan_subpass_command::draw_indexed;
	cmd.draw_indexed_data.index_count = index_count;
	cmd.draw_indexed_data.instance_count = instance_count;
	cmd.draw_indexed_data.first_index = first_index;
	cmd.draw_indexed_data.vertex_offset = vertex_offset;
	cmd.draw_indexed_data.first_instance = first_instance;
}

void vulkan_subpass_instance::record_bind_pipeline(
	VkCommandBuffer cmd_buf,
	const vulkan_subpass_command& cmd,
	const VkViewport& viewport,
	const VkRect2D& scissor
) {
	if (pipelines[cmd.pipeline_index]->handle() ==
		VK_NULL_HANDLE) {
		pipelines[cmd.pipeline_index]->init();
	}
	vkCmdBindPipeline(
		cmd_buf,
		VK_PIPELINE_BIND_POINT_GRAPHICS,
		pipelines[cmd.pipeline_index]->handle()
	);
	vkCmdSetViewport(cmd_buf, 0, 1, &viewport);
	vkCmdSetScissor(cmd_buf, 0, 1, &scissor);
}

void vulkan_subpass_instance::record_bind_vertex_buffers(
	VkCommandBuffer cmd_buf,
	const vulkan_subpass_command& cmd
) {
	auto& bindings =
		vertex_buffer_bindings[cmd.vertex_buffers_index];
	vector<VkBuffer> buffers;
	for (auto& b : bindings.buffers) {
		buffers.push_back(b->handle());
	}
	vkCmdBindVertexBuffers(
		cmd_buf,
		bindings.first_binding,
		static_cast<uint32_t>(buffers.size()),
		buffers.data(),
		bindings.offsets.data()
	);
}

void vulkan_subpass_instance::record_bind_index_buffer(
	VkCommandBuffer cmd_buf,
	const vulkan_subpass_command& cmd
) {
	auto& binding = index_buffer_bindings[cmd.index_buffer_index];
	vkCmdBindIndexBuffer(
		cmd_buf,
		binding.buffer->handle(),
		binding.offset,
		binding.index_type == index_type::u16
		? VK_INDEX_TYPE_UINT16 : VK_INDEX_TYPE_UINT32
	);
}

void vulkan_subpass_instance::record_draw(
	VkCommandBuffer cmd_buf,
	const vulkan_subpass_command& cmd
) {
	vkCmdDraw(
		cmd_buf,
		cmd.draw_data.vertex_count,
		cmd.draw_data.instance_count,
		cmd.draw_data.first_vertex,
		cmd.draw_data.first_instance
	);
}

void vulkan_subpass_instance::record_draw_indexed(
	VkCommandBuffer cmd_buf,
	const vulkan_subpass_command& cmd
) {
	vkCmdDrawIndexed(
		cmd_buf,
		cmd.draw_indexed_data.index_count,
		cmd.draw_indexed_data.instance_count,
		cmd.draw_indexed_data.first_index,
		cmd.draw_indexed_data.vertex_offset,
		cmd.draw_indexed_data.first_instance
	);
}

}