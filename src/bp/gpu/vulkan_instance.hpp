#pragma once
#include "vulkan_gpu.hpp"
#include <bp/gpu/instance.hpp>
#include <vulkan/vulkan.h>
#include <vector>
#include <memory>

#include <bp/util/api.hpp>

namespace bp {

class BP_API vulkan_instance :
	public instance,
	public std::enable_shared_from_this<vulkan_instance> {
	VkInstance m_handle;
	std::vector<vulkan_gpu> vulkan_gpus;
	VkDebugReportCallbackEXT debug_report_callback;

public:
	vulkan_instance() : vulkan_instance{{}} {}
	vulkan_instance(std::vector<const char*> extensions);
	~vulkan_instance();

	std::shared_ptr<gpu_query> create_gpu_query() override;

	std::shared_ptr<context> create_context(
		const bp::gpu& gpu,
		gpu_usage usage
	) override;

	VkInstance handle() { return m_handle; }

	const vulkan_gpu& gpu(unsigned id) const { return vulkan_gpus[id]; }
};

}

#undef BP_API