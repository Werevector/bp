#pragma once
#include <vulkan/vulkan.h>
#include <functional>

#include <bp/util/api.hpp>

namespace bp {

class vulkan_command_pool;

class BP_API vulkan_command_buffer {
	friend class vulkan_queue;

	VkCommandBuffer m_handle;
	std::function<void(VkCommandBuffer)> deleter;
public:
	vulkan_command_buffer(
		VkCommandBuffer handle,
		std::function<void(VkCommandBuffer)> deleter
	);
	~vulkan_command_buffer();

	VkCommandBuffer handle() { return m_handle; }
};

}

#undef BP_API