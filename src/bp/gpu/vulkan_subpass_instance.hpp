#pragma once
#include <bp/gpu/subpass_instance.hpp>
#include "vulkan_subpass_command.hpp"
#include <memory>
#include <future>
#include <vector>

#include <bp/util/api.hpp>

namespace bp {

class vulkan_render_pass_instance;
class vulkan_subpass;
class vulkan_command_buffer;
class vulkan_queue;
class vulkan_graphics_pipeline;
struct vulkan_subpass_command;
class vulkan_buffer;

class BP_API vulkan_subpass_instance : public subpass_instance {
	friend class vulkan_render_pass_instance;
	std::weak_ptr<vulkan_render_pass_instance> render_pass_instance;
	std::shared_ptr<vulkan_subpass> subpass;
	std::shared_ptr<vulkan_command_buffer> cmd_buffer;
	VkCommandBufferInheritanceInfo inheritance_info;

	std::vector<vulkan_subpass_command> commands;

	//Command data
	std::vector<std::shared_ptr<vulkan_graphics_pipeline>> pipelines;
	struct vertex_buffer_bindings {
		std::vector<std::shared_ptr<vulkan_buffer>> buffers;
		std::vector<size_t> offsets;
		uint32_t first_binding;
	};
	std::vector<vertex_buffer_bindings> vertex_buffer_bindings;
	struct index_buffer_binding {
		std::shared_ptr<vulkan_buffer> buffer;
		size_t offset;
		bp::index_type index_type;
	};
	std::vector<index_buffer_binding> index_buffer_bindings;

public:
	vulkan_subpass_instance(
		std::shared_ptr<vulkan_render_pass_instance> render_pass_instance,
		std::shared_ptr<vulkan_subpass> subpass
	);

	std::future<std::shared_ptr<vulkan_command_buffer>> prepare(
		std::shared_ptr<vulkan_queue> queue,
		VkViewport viewport,
		VkRect2D scissor
	);

	void bind_pipeline(std::shared_ptr<bp::pipeline> pipeline) override;

	void push_constants(
		uint32_t offset,
		uint32_t size,
		const void* data
	) override;

	void bind_vertex_buffers(
		std::vector<vertex_buffer_binding> bindings,
		uint32_t first_binding
	) override;

	void bind_index_buffer(
		std::shared_ptr<bp::buffer> buffer,
		size_t offset,
		bp::index_type index_type
	) override;

	void draw(
		uint32_t vertex_count,
		uint32_t instance_count,
		uint32_t first_vertex,
		uint32_t first_instance
	) override;

	void draw_indexed(
		uint32_t index_count,
		uint32_t instance_count,
		uint32_t first_index,
		int32_t vertex_offset,
		uint32_t first_instance
	) override;

private:
	void record_bind_pipeline(
		VkCommandBuffer cmd_buf,
		const vulkan_subpass_command& cmd,
		const VkViewport& viewport,
		const VkRect2D& scissor
	);

	void record_bind_vertex_buffers(
		VkCommandBuffer cmd_buf,
		const vulkan_subpass_command& cmd
	);

	void record_bind_index_buffer(
		VkCommandBuffer cmd_buf,
		const vulkan_subpass_command& cmd
	);

	void record_draw(
		VkCommandBuffer cmd_buf,
		const vulkan_subpass_command& cmd
	);

	void record_draw_indexed(
		VkCommandBuffer cmd_buf,
		const vulkan_subpass_command& cmd
	);
};

}

#undef BP_API