#define BP_API_IMPL
#include "vulkan_texture.hpp"
#include "vulkan_buffer.hpp"
#include "vulkan_context.hpp"
#include "vulkan_attachment.hpp"
#include <bp/gpu/gpu_error.hpp>

using namespace std;

namespace bp {

static size_t format_size(texture_format format)
{
	switch (format)
	{
	case texture_format::r8: return 1;
	case texture_format::r8g8:
	case texture_format::depth: return 2;
	case texture_format::r8g8b8a8: return 4;
	default: return 0;
	}
}


void vulkan_texture::create() {
	//Create image and allocate image memory
	image_info.extent.width = static_cast<uint32_t>(m_width);
	image_info.extent.height = static_cast<uint32_t>(m_height);
	image_info.extent.depth = static_cast<uint32_t>(m_depth);

	VmaAllocationCreateInfo allocation_create_info;
	allocation_create_info.flags = 0;
	allocation_create_info.usage = memory_usage;
	allocation_create_info.requiredFlags = 0;
	allocation_create_info.preferredFlags = 0;
	allocation_create_info.memoryTypeBits = 0;
	allocation_create_info.pool = VK_NULL_HANDLE;
	allocation_create_info.pUserData = nullptr;

	VmaAllocationInfo allocation_info;
	VkResult result = vmaCreateImage(
		context->memory_allocator(),
		&image_info,
		&allocation_create_info,
		&image,
		&memory_allocation,
		&allocation_info
	);
	if (result != VK_SUCCESS) {
		throw gpu_error{
			"Failed to create image."
		};
	}

	vmaGetMemoryTypeProperties(
		context->memory_allocator(),
		allocation_info.memoryType,
		&memory_property_flags
	);

	//Create image view
	VkImageViewCreateInfo viewInfo;
	viewInfo.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
	viewInfo.pNext = nullptr;
	viewInfo.flags = 0;
	viewInfo.image = image;
	viewInfo.viewType =
		image_info.imageType == VK_IMAGE_TYPE_2D
		? VK_IMAGE_VIEW_TYPE_2D : VK_IMAGE_VIEW_TYPE_3D;
	viewInfo.format = image_info.format;
	viewInfo.components.r = VK_COMPONENT_SWIZZLE_IDENTITY;
	viewInfo.components.g = VK_COMPONENT_SWIZZLE_IDENTITY;
	viewInfo.components.b = VK_COMPONENT_SWIZZLE_IDENTITY;
	viewInfo.components.a = VK_COMPONENT_SWIZZLE_IDENTITY;
	viewInfo.subresourceRange.aspectMask =
		m_format == texture_format::depth
		? VK_IMAGE_ASPECT_DEPTH_BIT : VK_IMAGE_ASPECT_COLOR_BIT;
	viewInfo.subresourceRange.baseMipLevel = 0;
	viewInfo.subresourceRange.levelCount = 1;
	viewInfo.subresourceRange.baseArrayLayer = 0;
	viewInfo.subresourceRange.layerCount = 1;

	result = vkCreateImageView(
		context->device(),
		&viewInfo,
		nullptr,
		&view
	);
	if (result != VK_SUCCESS) {
		vkDestroyImage(context->device(), image, nullptr);
		vmaFreeMemory(context->memory_allocator(), memory_allocation);
		throw gpu_error{
			"Failed to create image view."
		};
	}

	mapped = nullptr;
	layout = image_info.initialLayout;
	access_flags = 0;
	transition_barrier.image = image;
}

void vulkan_texture::destroy() {
	staging_buffer.reset();
	vkDestroyImageView(context->device(), view, nullptr);
	vkDestroyImage(context->device(), image, nullptr);
	vmaFreeMemory(context->memory_allocator(), memory_allocation);
}

vulkan_texture::vulkan_texture(
	shared_ptr<vulkan_context> context,
	texture_type type,
	texture_format format,
	texture_usage usage,
	size_t width,
	size_t height,
	size_t depth,
	VmaMemoryUsage memory_usage,
	VkImageTiling image_tiling
) : context{context},
	m_type{type},
	m_format{format},
	m_usage{usage},
	m_width{width},
	m_height{height},
	m_depth{depth},
	m_size{format_size(format) * width * height * depth},
	memory_usage{memory_usage},
	mapped{nullptr},
	layout{VK_IMAGE_LAYOUT_UNDEFINED},
	access_flags{0} {

	//Setup image create info and transition barrier structs
	image_info.sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO;
	image_info.pNext = nullptr;
	image_info.flags = 0;

	switch (type) {
	case texture_type::texture_2D:
		image_info.imageType = VK_IMAGE_TYPE_2D;
		break;
	case texture_type::texture_3D:
		image_info.imageType = VK_IMAGE_TYPE_3D;
		break;
	}

	switch (format) {
	case texture_format::r8:
		image_info.format = VK_FORMAT_R8_UNORM;
		break;
	case texture_format::r8g8:
		image_info.format = VK_FORMAT_R8G8_UNORM;
		break;
	case texture_format::r8g8b8a8:
		image_info.format = VK_FORMAT_R8G8B8A8_UNORM;
		break;
	case texture_format::depth:
		image_info.format = VK_FORMAT_D16_UNORM;
		break;
	}

	image_info.mipLevels = 1;
	image_info.arrayLayers = 1;
	image_info.samples = VK_SAMPLE_COUNT_1_BIT;
	image_info.tiling = image_tiling;

	image_info.usage = VK_IMAGE_USAGE_TRANSFER_SRC_BIT
					   | VK_IMAGE_USAGE_TRANSFER_DST_BIT;
	if (usage & texture_usage::attachment) {
		image_info.usage |=
			format == texture_format::depth
			? VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT
			: VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT;
	}
	if (usage & texture_usage::sampled) {
		image_info.usage |= VK_IMAGE_USAGE_SAMPLED_BIT;
	}

	image_info.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
	image_info.queueFamilyIndexCount = 0;
	image_info.pQueueFamilyIndices = nullptr;
	image_info.initialLayout = layout;

	transition_barrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
	transition_barrier.pNext = nullptr;
	transition_barrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
	transition_barrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
	transition_barrier.subresourceRange = {
		m_format == texture_format::depth
			? static_cast<VkImageAspectFlags>(VK_IMAGE_ASPECT_DEPTH_BIT)
			: static_cast<VkImageAspectFlags>(VK_IMAGE_ASPECT_COLOR_BIT),
		0, 1, 0, 1
	};

	create();
}

vulkan_texture::~vulkan_texture() {
	destroy();
}

void vulkan_texture::resize(size_t width, size_t height, size_t depth) {
	destroy();
	m_width = width;
	m_height = height;
	m_depth = depth;
	m_size = format_size(m_format) * width * height * depth;
	create();
}

uint8_t* vulkan_texture::map() {
	if (mapped) return mapped;

	if (image_info.tiling == VK_IMAGE_TILING_LINEAR
		&& memory_property_flags & VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT) {
		VkResult result = vmaMapMemory(
			context->memory_allocator(),
			memory_allocation,
			reinterpret_cast<void**>(&mapped)
		);

		if (result != VK_SUCCESS) {
			throw gpu_error{
				"Failed to map gpu memory."
			};
		}
	} else {
		if (!staging_buffer) {
			staging_buffer = make_unique<vulkan_buffer>(
				context,
				buffer_usage{},
				m_size,
				VMA_MEMORY_USAGE_CPU_ONLY
			);
		}
		mapped = staging_buffer->map();
	}

	return mapped;
}

void vulkan_texture::unmap() {
	if (mapped == nullptr) return;

	if (staging_buffer) {
		staging_buffer->unmap();
	} else {
		vmaUnmapMemory(
			context->memory_allocator(),
			memory_allocation
		);
	}

	mapped = nullptr;
}

const VkImageMemoryBarrier& vulkan_texture::transition(
	VkImageLayout dst_layout,
	VkAccessFlags dst_access
) {
	transition_barrier.srcAccessMask = access_flags;
	transition_barrier.dstAccessMask = dst_access;
	transition_barrier.oldLayout = layout;
	transition_barrier.newLayout = dst_layout;

	layout = dst_layout;
	access_flags = dst_access;

	return transition_barrier;
}

optional<VkImageMemoryBarrier> vulkan_texture::before_transition(
	shared_ptr<vulkan_attachment> attachment
) {
	VkImageLayout dst_layout;
	VkAccessFlags dst_access;

	if (attachment->flags() & vulkan_attachment::color_attachment) {
		dst_layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;
		dst_access = VK_ACCESS_COLOR_ATTACHMENT_READ_BIT
					 | VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
	} else if (attachment->flags() & vulkan_attachment::depth_attachment) {
		dst_layout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;
		dst_access = VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_READ_BIT
					 | VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT;
	} else if (attachment->flags() & vulkan_attachment::input_attachment) {
		dst_layout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
		dst_access = VK_ACCESS_INPUT_ATTACHMENT_READ_BIT;
	} else {
		return nullopt;
	}

	if (dst_layout == layout || dst_access == access_flags) {
		return nullopt;
	}

	return optional<VkImageMemoryBarrier>{transition(dst_layout, dst_access)};
}

VkBuffer vulkan_texture::staging_buffer_handle() {
	return staging_buffer ? staging_buffer->handle() : VK_NULL_HANDLE;
}

}