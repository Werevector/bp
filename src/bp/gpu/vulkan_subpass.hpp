#pragma once
#include <bp/gpu/subpass.hpp>
#include <memory>
#include <vector>
#include <vulkan/vulkan.h>

#include <bp/util/api.hpp>

namespace bp {

class vulkan_attachment;
class vulkan_render_pass;
class vulkan_subpass_instance;
class vulkan_graphics_pipeline;

class BP_API vulkan_subpass : public subpass {
	friend class vulkan_render_pass;
	friend class vulkan_subpass_instance;
	friend class vulkan_graphics_pipeline;

	struct subpass_dependency {
		std::weak_ptr<vulkan_subpass> src;
		VkPipelineStageFlags src_stage;
		VkPipelineStageFlags dst_stage;
	};

	enum {
		source_subpass = 0x0001,
		visited = 0x0002
	};

	int state{source_subpass};
	uint32_t index{UINT32_MAX};
	std::weak_ptr<vulkan_render_pass> render_pass;
	std::vector<std::shared_ptr<vulkan_attachment>> input_attachments;
	std::vector<std::shared_ptr<vulkan_attachment>> color_attachments;
	std::vector<std::shared_ptr<vulkan_attachment>> resolve_attachments;
	std::shared_ptr<vulkan_attachment> depth_attachment;
	std::vector<std::shared_ptr<vulkan_attachment>> preserve_attachments;
	std::vector<subpass_dependency> dependencies;
	std::vector<std::shared_ptr<vulkan_subpass>> dependents;

public:
	vulkan_subpass(std::weak_ptr<vulkan_render_pass> render_pass) :
		render_pass{render_pass} {}

	void add_input_attachment(std::shared_ptr<attachment> input) override;

	void add_color_attachment(
		std::shared_ptr<attachment> color,
		std::shared_ptr<attachment> resolve
	) override;

	void set_depth_attachment(std::shared_ptr<attachment> depth) override;

	void add_preserve_attachment(std::shared_ptr<attachment> preserve) override;

	void add_dependency(
		std::shared_ptr<subpass> src,
		pipeline_stage src_stage,
		pipeline_stage dst_stage
	) override;
};

}