#define BP_API_IMPL
#include "vulkan_instance.hpp"
#include "vulkan_gpu_query.hpp"
#include "vulkan_context.hpp"
#include <sstream>
#include <iostream>

using namespace std;

namespace bp {

#ifndef NDEBUG

static const char* VALIDATION_LAYER = "VK_LAYER_LUNARG_standard_validation";

static VKAPI_ATTR VkBool32
VKAPI_CALL debug_callback(
	VkDebugReportFlagsEXT flags,
	VkDebugReportObjectTypeEXT,
	uint64_t,
	size_t,
	int32_t,
	const char* pLayerPrefix,
	const char* pMessage,
	void*
) {
	stringstream ss;
	ss << pLayerPrefix << ": " << pMessage << endl;

	if (flags & (VK_DEBUG_REPORT_INFORMATION_BIT_EXT
				 | VK_DEBUG_REPORT_DEBUG_BIT_EXT))
		cout << ss.str();
	else
		cerr << ss.str();

	return VK_FALSE;
}

#endif

vulkan_instance::vulkan_instance(std::vector<const char*> extensions) {
	VkInstanceCreateInfo instance_info = {};
	instance_info.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
	instance_info.pApplicationInfo = nullptr;

	vector<const char*> layers;
#ifndef NDEBUG
	extensions.push_back(VK_EXT_DEBUG_REPORT_EXTENSION_NAME);
	layers.push_back(VALIDATION_LAYER);
#endif

	instance_info.enabledExtensionCount =
		static_cast<uint32_t>(extensions.size());
	instance_info.ppEnabledExtensionNames = extensions.data();
	instance_info.enabledLayerCount = static_cast<uint32_t>(layers.size());
	instance_info.ppEnabledLayerNames = layers.data();

	VkResult result = vkCreateInstance(&instance_info, nullptr, &m_handle);

	if (result != VK_SUCCESS)
		throw runtime_error("Failed to create instance.");

	uint32_t n = 0;
	vkEnumeratePhysicalDevices(m_handle, &n, nullptr);
	vector<VkPhysicalDevice> physical_devices{n};
	vkEnumeratePhysicalDevices(m_handle, &n, physical_devices.data());

	for (VkPhysicalDevice d : physical_devices) {
		vulkan_gpus.emplace_back();
		vulkan_gpu& gpu = vulkan_gpus.back();
		gpu.physical_device = d;
		vkGetPhysicalDeviceProperties(d, &gpu.properties);
		vkGetPhysicalDeviceMemoryProperties(d, &gpu.memory_properties);

		vkGetPhysicalDeviceQueueFamilyProperties(d, &n, nullptr);
		gpu.queue_families.resize(n);
		vkGetPhysicalDeviceQueueFamilyProperties(
			d, &n, gpu.queue_families.data());

		vkEnumerateDeviceExtensionProperties(d, nullptr, &n, nullptr);
		gpu.extensions.resize(n);
		vkEnumerateDeviceExtensionProperties(
			d, nullptr, &n, gpu.extensions.data());
	}

#ifndef NDEBUG
	PFN_vkCreateDebugReportCallbackEXT vkCreateDebugReportCallback =
		reinterpret_cast<PFN_vkCreateDebugReportCallbackEXT>(
			vkGetInstanceProcAddr(
				m_handle, "vkCreateDebugReportCallbackEXT"
			));

	VkDebugReportCallbackCreateInfoEXT debug_info = {};
	debug_info.sType =
		VK_STRUCTURE_TYPE_DEBUG_REPORT_CALLBACK_CREATE_INFO_EXT;
	debug_info.flags = VK_DEBUG_REPORT_ERROR_BIT_EXT
					   | VK_DEBUG_REPORT_WARNING_BIT_EXT
					   | VK_DEBUG_REPORT_PERFORMANCE_WARNING_BIT_EXT;
	debug_info.pfnCallback = debug_callback;
	debug_info.pUserData = static_cast<void*>(this);

	result = vkCreateDebugReportCallback(
		m_handle, &debug_info, nullptr,
		&debug_report_callback
	);

	if (result != VK_SUCCESS) {
		vkDestroyInstance(m_handle, nullptr);
		m_handle = VK_NULL_HANDLE;
		throw runtime_error("Failed to create debug report callback.");
	}
#endif
}

vulkan_instance::~vulkan_instance() {
#ifndef NDEBUG
	PFN_vkDestroyDebugReportCallbackEXT vkDestroyDebugReportCallback =
		reinterpret_cast<PFN_vkDestroyDebugReportCallbackEXT>(
			vkGetInstanceProcAddr(
				m_handle, "vkDestroyDebugReportCallbackEXT"
			));
	vkDestroyDebugReportCallback(m_handle, debug_report_callback, nullptr);
#endif
	vkDestroyInstance(m_handle, nullptr);
}

shared_ptr<gpu_query> vulkan_instance::create_gpu_query() {
	return make_shared<vulkan_gpu_query>(vulkan_gpus);
}

shared_ptr<context> vulkan_instance::create_context(
	const bp::gpu& gpu,
	gpu_usage usage
) {
	return make_shared<vulkan_context>(
		shared_from_this(),
		gpu,
		usage
	);
}

#define BP_API_IMPL
#include <bp/util/api.hpp>
BP_API std::shared_ptr<instance> create_instance() {
	return make_shared<vulkan_instance>();
}
#undef BP_API

}
