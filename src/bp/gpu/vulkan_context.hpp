#pragma once
#include "vk_mem_alloc.h"
#include <bp/gpu/context.hpp>
#include <bp/gpu/gpu.hpp>
#include <vulkan/vulkan.h>

#include <bp/util/api.hpp>

namespace bp {

class vulkan_instance;
class vulkan_queue;
class vulkan_semaphore;

class BP_API vulkan_context :
	public context,
	public std::enable_shared_from_this<vulkan_context> {
	std::shared_ptr<vulkan_instance> instance;
	VkPhysicalDevice m_physical_device;
	VkDevice m_device;
	std::shared_ptr<vulkan_queue> m_transfer_queue;
	std::shared_ptr<vulkan_queue> m_graphics_queue;
	std::shared_ptr<vulkan_queue> m_compute_queue;
	VmaAllocator m_memory_allocator;

public:
	vulkan_context(
		std::shared_ptr<vulkan_instance> instance,
		const bp::gpu& gpu,
		gpu_usage usage
	);

	~vulkan_context();

	VkPhysicalDevice physical_device() const { return m_physical_device; }

	VkDevice device() { return m_device; }

	std::shared_ptr<vulkan_queue> transfer_queue() { return m_transfer_queue; }

	std::shared_ptr<vulkan_queue> graphics_queue() { return m_graphics_queue; }

	std::shared_ptr<vulkan_queue> compute_queue() { return m_compute_queue; }

	VmaAllocator memory_allocator() { return m_memory_allocator; }

	std::shared_ptr<buffer> create_buffer(
		buffer_usage usage,
		std::size_t size
	) override;

	std::shared_ptr<texture> create_texture(
		texture_type type,
		texture_format format,
		texture_usage usage,
		std::size_t width,
		std::size_t height,
		std::size_t depth
	) override;

	std::shared_ptr<swapchain> create_swapchain(
		std::shared_ptr<surface> target,
		swapchain_flags flags,
		std::size_t width,
		std::size_t height
	) override;

	std::shared_ptr<render_pass> create_render_pass() override;

	std::shared_ptr<framebuffer> create_framebuffer(
		std::shared_ptr<bp::render_pass> render_pass,
		std::size_t width,
		std::size_t height
	) override;

	std::shared_ptr<bp::batch> create_batch() override;

	std::shared_ptr<shader> create_shader(
		shader_stage stage,
		std::size_t spirv_code_size,
		const std::uint8_t* spirv_code
	) override;

	std::shared_ptr<pipeline_layout> create_pipeline_layout() override;

	std::shared_ptr<pipeline> create_graphics_pipeline(
		std::shared_ptr<pipeline_layout> layout,
		std::vector<std::shared_ptr<shader>> shaders,
		std::shared_ptr<bp::subpass> subpass,
		std::vector<vertex_input_binding> vertex_bindings,
		std::vector<vertex_input_attribute> vertex_attributes,
		primitive_topology topology,
		bp::polygon_mode polygon_mode,
		graphics_pipeline_flags flags
	) override;

	std::shared_ptr<vulkan_semaphore> create_semaphore();
};

}

#undef BP_API