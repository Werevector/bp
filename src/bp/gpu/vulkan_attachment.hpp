#pragma once
#include <bp/gpu/attachment.hpp>
#include <vulkan/vulkan.h>
#include <memory>

#include <bp/util/api.hpp>

namespace bp {

class vulkan_render_pass;
class vulkan_subpass;
class vulkan_framebuffer;

class BP_API vulkan_attachment : public attachment {
	friend class vulkan_render_pass;
	friend class vulkan_subpass;
	friend class vulkan_framebuffer;

	std::weak_ptr<vulkan_render_pass> render_pass;
	uint32_t m_index;
	attachment_flags m_flags;
	VkFormat m_format;
	VkClearValue m_clear_value;

public:
	enum {
		color_attachment = 0x0008,
		depth_attachment = 0x0010,
		input_attachment = 0x0020
	};

	vulkan_attachment(
		std::weak_ptr<vulkan_render_pass> render_pass,
		uint32_t index,
		attachment_flags flags
	) : render_pass{render_pass},
		m_index{index},
		m_flags{flags},
		m_format{VK_FORMAT_UNDEFINED} {

	}

	uint32_t index() const { return m_index; }
	attachment_flags flags() const override { return m_flags; }
	VkFormat format() const { return m_format; }
	void format(VkFormat fmt) { m_format = fmt; }
	const VkClearValue& clear_value() const { return m_clear_value; }
	void clear_color(float r, float g, float b, float a) override {
		if (m_flags & attachment_flags::clear
			&& m_format != VK_FORMAT_D16_UNORM) {
			m_clear_value = {r, g, b, a};
		}
	}
};

}

#undef BP_API