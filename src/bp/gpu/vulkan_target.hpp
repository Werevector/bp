#pragma once
#include <optional>
#include <memory>
#include <cstdint>
#include <vulkan/vulkan.h>

#include <bp/util/api.hpp>

namespace bp {

class vulkan_attachment;

class BP_API vulkan_target {
public:
	virtual ~vulkan_target() = default;

	virtual void resize_target(std::size_t width, std::size_t height) = 0;

	virtual std::optional<VkImageMemoryBarrier> before_transition(
		std::shared_ptr<vulkan_attachment> attachment
	) {
		return std::nullopt;
	}

	virtual std::optional<VkImageMemoryBarrier> after_transition(
		std::shared_ptr<vulkan_attachment> attachment
	) {
		return std::nullopt;
	}
};

}

#undef BP_API