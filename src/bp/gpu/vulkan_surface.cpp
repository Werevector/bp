#define BP_API_IMPL
#include "vulkan_surface.hpp"
#include <stdexcept>

namespace bp {

vulkan_surface::vulkan_surface(
	std::shared_ptr<vulkan_instance> instance,
	VkSurfaceKHR handle
) :
	m_handle{handle} {
	if (handle == VK_NULL_HANDLE) {
		throw std::invalid_argument(
			"Surface handle can not be VK_NULL_HANDLE"
		);
	}
}

}