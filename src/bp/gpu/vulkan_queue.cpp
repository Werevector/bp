#define BP_API_IMPL
#include "vulkan_queue.hpp"
#include "vulkan_command_pool.hpp"
#include "vulkan_command_buffer.hpp"
#include <thread>

using namespace std;

namespace bp {

vulkan_queue::vulkan_queue(
	VkDevice device,
	uint32_t family_index,
	uint32_t index
) : device{device},
	family_index{family_index},
	index{index},
	cmd_pools_sem{thread::hardware_concurrency()} {
	vkGetDeviceQueue(device, family_index, index, &m_handle);
}

future<shared_ptr<vulkan_command_buffer>> vulkan_queue::record_commands(
	VkCommandBufferLevel level,
	std::function<void(VkCommandBuffer)> record,
	VkCommandBufferUsageFlags usage_flags,
	const VkCommandBufferInheritanceInfo* inheritance_info
) {
	return async(
		launch::async,
		[
			this,
			level,record,
			usage_flags,
			inheritance_info
		] {
			shared_ptr<vulkan_command_pool> cmd_pool;
			cmd_pools_sem.wait();
			{
				unique_lock<mutex> lk{cmd_pool_queue_mutex};
				if (!cmd_pool_queue.empty()) {
					cmd_pool = cmd_pool_queue.front();
					cmd_pool_queue.pop();
				}
			}
			if (!cmd_pool) {
				cmd_pool = make_shared<vulkan_command_pool>(
					device,
					family_index
				);
			}

			auto cmd_buf = cmd_pool->allocate_command_buffer(level);
			{
				unique_lock<mutex> lk{cmd_pool->mutex()};
				VkCommandBufferBeginInfo begin_info;
				begin_info.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
				begin_info.pNext = nullptr;
				begin_info.flags = usage_flags;
				begin_info.pInheritanceInfo = inheritance_info;
				vkBeginCommandBuffer(cmd_buf->handle(), &begin_info);
				record(cmd_buf->handle());
				vkEndCommandBuffer(cmd_buf->handle());
			}

			{
				unique_lock<mutex> lk{cmd_pool_queue_mutex};
				cmd_pool_queue.push(cmd_pool);
			}
			cmd_pools_sem.notify();
			return cmd_buf;
		}
	);
}

}