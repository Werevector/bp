#define BP_API_IMPL
#include "vulkan_semaphore.hpp"
#include "vulkan_context.hpp"
#include <bp/gpu/gpu_error.hpp>

namespace bp {

vulkan_semaphore::vulkan_semaphore(
	std::shared_ptr<bp::vulkan_context> context
) : context{context} {
	VkSemaphoreCreateInfo info;
	info.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;
	info.pNext = nullptr;
	info.flags = 0;

	VkResult result = vkCreateSemaphore(
		context->device(),
		&info,
		nullptr,
		&m_handle
	);
	if (result != VK_SUCCESS) {
		throw gpu_error{
			"Failed to create semaphore."
		};
	}
}

vulkan_semaphore::~vulkan_semaphore() {
	vkDestroySemaphore(context->device(), m_handle, nullptr);
}

}