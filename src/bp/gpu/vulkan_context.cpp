#define BP_API_IMPL
#include "vulkan_context.hpp"
#include "vulkan_instance.hpp"
#include "vulkan_queue.hpp"
#include "vulkan_buffer.hpp"
#include "vulkan_texture.hpp"
#include "vulkan_surface.hpp"
#include "vulkan_swapchain.hpp"
#include "vulkan_shader.hpp"
#include "vulkan_pipeline_layout.hpp"
#include "vulkan_graphics_pipeline.hpp"
#include "vulkan_render_pass.hpp"
#include "vulkan_subpass.hpp"
#include "vulkan_framebuffer.hpp"
#include "vulkan_semaphore.hpp"
#include "vulkan_batch.hpp"
#include <bp/gpu/gpu_error.hpp>
#include <stdexcept>

using namespace std;

namespace bp {

vulkan_context::vulkan_context(
	shared_ptr<vulkan_instance> instance,
	const gpu& gpu,
	gpu_usage usage
) : instance{instance} {
	const vulkan_gpu& vgpu = instance->gpu(gpu.id());
	m_physical_device = vgpu.physical_device;

	//Setup the needed queue create infos
	VkQueueFlags found_queues = 0;
	vector<VkDeviceQueueCreateInfo> queue_infos;
	const float queue_priority = 1.f;

	//Find the queue family for transfer
	unsigned transfer_qf = 0;
	for (unsigned i = 0; i < vgpu.queue_families.size(); i++) {
		VkQueueFlags flags = vgpu.queue_families[i].queueFlags;

		if (flags == VK_QUEUE_TRANSFER_BIT) {
			//Assume that a transfer-only queue family is most
			//efficient at transfer operations
			transfer_qf = i;
			found_queues |= VK_QUEUE_TRANSFER_BIT;
			break;
		}

		if ((flags & VK_QUEUE_TRANSFER_BIT)
			&& (!(flags & VK_QUEUE_GRAPHICS_BIT)
				|| !(flags & VK_QUEUE_COMPUTE_BIT))) {
			//Assume that non graphics or compute queues are more
			//efficient at transfer operations than graphics or
			//compute queues
			transfer_qf = i;
			found_queues |= VK_QUEUE_TRANSFER_BIT;
			continue;
		}

		if (!(found_queues & VK_QUEUE_TRANSFER_BIT)
			&& flags & (VK_QUEUE_TRANSFER_BIT | VK_QUEUE_GRAPHICS_BIT |
						VK_QUEUE_COMPUTE_BIT)) {
			transfer_qf = i;
			found_queues |= VK_QUEUE_TRANSFER_BIT;
		}
	}

	if (!(found_queues & VK_QUEUE_TRANSFER_BIT)) {
		throw gpu_error{
			"No transfer capable queue family found."
		};
	}

	{
		queue_infos.emplace_back();
		auto& queue_info = queue_infos.back();
		queue_info.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
		queue_info.pNext = nullptr;
		queue_info.flags = 0;
		queue_info.queueFamilyIndex = transfer_qf;
		queue_info.queueCount = 1;
		queue_info.pQueuePriorities = &queue_priority;
	}

	//Find the queue family for graphics if needed
	unsigned graphics_qf = 0;
	if (usage & gpu_usage::graphics) {
		for (unsigned i = 0; i < vgpu.queue_families.size(); i++) {
			if (vgpu.queue_families[i].queueFlags & VK_QUEUE_GRAPHICS_BIT) {
				graphics_qf = i;
				found_queues |= VK_QUEUE_GRAPHICS_BIT;
				if (graphics_qf != queue_infos[0].queueFamilyIndex) {
					break;
				}
			}
		}

		if (!(found_queues & VK_QUEUE_GRAPHICS_BIT)) {
			throw gpu_error{
				"No graphics capable queue family found."
			};
		}

		if (graphics_qf != transfer_qf) {
			queue_infos.emplace_back();
			auto& queue_info = queue_infos.back();
			queue_info.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
			queue_info.pNext = nullptr;
			queue_info.flags = 0;
			queue_info.queueFamilyIndex = graphics_qf;
			queue_info.queueCount = 1;
			queue_info.pQueuePriorities = &queue_priority;
		}
	}


	//Find the queue family for compute if needed
	unsigned compute_qf = 0;
	if (usage & gpu_usage::compute) {
		for (unsigned i = 0; i < vgpu.queue_families.size(); i++) {
			VkQueueFlags flags = vgpu.queue_families[i].queueFlags;

			if (flags == VK_QUEUE_COMPUTE_BIT) {
				//Assume that a compute only queue is most
				//efficient at compute operations
				compute_qf = i;
				found_queues |= VK_QUEUE_COMPUTE_BIT;
				break;
			}

			if (flags & VK_QUEUE_COMPUTE_BIT
				&& !(flags & VK_QUEUE_GRAPHICS_BIT)) {
				//Assume that non-graphics compute queues are
				//more efficient at compute operations than
				//graphics capable queues
				compute_qf = i;
				found_queues |= VK_QUEUE_COMPUTE_BIT;
				continue;
			}

			if (!(found_queues & VK_QUEUE_COMPUTE_BIT)
				&& flags & VK_QUEUE_COMPUTE_BIT) {
				compute_qf = i;
				found_queues |= VK_QUEUE_COMPUTE_BIT;
			}
		}

		if (!(found_queues & VK_QUEUE_COMPUTE_BIT)) {
			throw gpu_error{
				"No compute capable queue family found."
			};
		}

		if (compute_qf != transfer_qf
			&& (!(usage & gpu_usage::graphics)
				|| compute_qf != graphics_qf)) {
			queue_infos.emplace_back();
			auto& queue_info = queue_infos.back();
			queue_info.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
			queue_info.pNext = nullptr;
			queue_info.flags = 0;
			queue_info.queueFamilyIndex = compute_qf;
			queue_info.queueCount = 1;
			queue_info.pQueuePriorities = &queue_priority;
		}
	}

	//Create the logical device
	VkDeviceCreateInfo device_info;
	device_info.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;
	device_info.pNext = nullptr;
	device_info.flags = 0;
	device_info.queueCreateInfoCount =
		static_cast<uint32_t>(queue_infos.size());
	device_info.pQueueCreateInfos = queue_infos.data();
	device_info.enabledLayerCount = 0;
	device_info.ppEnabledLayerNames = nullptr;

	vector<const char*> device_extensions;
	VkPhysicalDeviceFeatures features = {};
	if (usage & gpu_usage::graphics) {
		if (usage & gpu_usage::surface) {
			device_extensions.push_back("VK_KHR_swapchain");
		}
		features.geometryShader = VK_TRUE;
		features.samplerAnisotropy = VK_TRUE;
	}

	device_info.enabledExtensionCount =
		static_cast<uint32_t>(device_extensions.size());
	device_info.ppEnabledExtensionNames = device_extensions.data();
	device_info.pEnabledFeatures = &features;

	VkResult result = vkCreateDevice(
		m_physical_device,
		&device_info,
		nullptr,
		&m_device
	);

	if (result != VK_SUCCESS) {
		throw gpu_error{
			"Failed to create logical device."
		};
	}

	m_transfer_queue = make_shared<vulkan_queue>(
		m_device,
		transfer_qf,
		0
	);

	if (usage & gpu_usage::graphics) {
		if (graphics_qf == transfer_qf) {
			m_graphics_queue = m_transfer_queue;
		} else {
			m_graphics_queue = make_shared<vulkan_queue>(
				m_device,
				graphics_qf,
				0
			);
		}
	}

	if (usage & gpu_usage::compute) {
		if (compute_qf == transfer_qf) {
			m_compute_queue = m_transfer_queue;
		} else if (usage & gpu_usage::graphics && compute_qf == graphics_qf) {
			m_compute_queue = m_graphics_queue;
		} else {
			m_compute_queue = make_shared<vulkan_queue>(
				m_device,
				compute_qf,
				0
			);
		}
	}

	//Create memory allocator
	VmaAllocatorCreateInfo allocator_info;
	allocator_info.flags = 0;
	allocator_info.physicalDevice = m_physical_device;
	allocator_info.device = m_device;
	allocator_info.preferredLargeHeapBlockSize = 0;
	allocator_info.pAllocationCallbacks = nullptr;
	allocator_info.pDeviceMemoryCallbacks = nullptr;
	allocator_info.frameInUseCount = 0;
	allocator_info.pHeapSizeLimit = nullptr;
	allocator_info.pVulkanFunctions = nullptr;
	allocator_info.pRecordSettings = nullptr;

	result = vmaCreateAllocator(
		&allocator_info,
		&m_memory_allocator
	);

	if (result != VK_SUCCESS) {
		throw gpu_error{
			"Failed to create gpu memory allocator."
		};
	}

}

vulkan_context::~vulkan_context() {
	m_transfer_queue.reset();
	m_graphics_queue.reset();
	m_compute_queue.reset();
	vmaDestroyAllocator(m_memory_allocator);
	vkDestroyDevice(m_device, nullptr);
}

shared_ptr<buffer> vulkan_context::create_buffer(
	buffer_usage usage,
	size_t size
) {
	return make_shared<vulkan_buffer>(
		shared_from_this(),
		usage,
		size,
		VMA_MEMORY_USAGE_GPU_ONLY
	);
}

shared_ptr<texture> vulkan_context::create_texture(
	texture_type type,
	texture_format format,
	texture_usage usage,
	size_t width,
	size_t height,
	size_t depth
) {
	return make_shared<vulkan_texture>(
		shared_from_this(),
		type,
		format,
		usage,
		width,
		height,
		depth,
		VMA_MEMORY_USAGE_GPU_ONLY,
		VK_IMAGE_TILING_OPTIMAL
	);
}

shared_ptr<swapchain> vulkan_context::create_swapchain(
	shared_ptr<surface> target,
	swapchain_flags flags,
	size_t width,
	size_t height
) {
	return make_shared<vulkan_swapchain>(
		shared_from_this(),
		static_pointer_cast<vulkan_surface>(target),
		flags,
		width,
		height
	);
}

shared_ptr<render_pass> vulkan_context::create_render_pass() {
	return make_shared<vulkan_render_pass>(shared_from_this());
}

shared_ptr<framebuffer> vulkan_context::create_framebuffer(
	shared_ptr<bp::render_pass> render_pass,
	size_t width,
	size_t height
) {
	return make_shared<vulkan_framebuffer>(
		shared_from_this(),
		static_pointer_cast<vulkan_render_pass>(render_pass),
		width,
		height
	);
}

shared_ptr<batch> vulkan_context::create_batch() {
	return make_shared<vulkan_batch>(
		shared_from_this()
	);
}

shared_ptr<shader> vulkan_context::create_shader(
	shader_stage stage,
	size_t spirv_code_size,
	const uint8_t* spirv_code
) {
	return make_shared<vulkan_shader>(
		shared_from_this(),
		stage,
		spirv_code_size,
		spirv_code
	);
}

shared_ptr<pipeline_layout> vulkan_context::create_pipeline_layout() {
	return make_shared<vulkan_pipeline_layout>(shared_from_this());
}

shared_ptr<pipeline> vulkan_context::create_graphics_pipeline(
	shared_ptr<pipeline_layout> layout,
	vector<shared_ptr<shader>> shaders,
	shared_ptr<bp::subpass> subpass,
	vector<vertex_input_binding> vertex_bindings,
	vector<vertex_input_attribute> vertex_attributes,
	primitive_topology topology,
	bp::polygon_mode polygon_mode,
	graphics_pipeline_flags flags
) {
	vector<shared_ptr<vulkan_shader>> vk_shaders;
	vk_shaders.reserve(shaders.size());
	for (auto s : shaders) {
		vk_shaders.push_back(static_pointer_cast<vulkan_shader>(s));
	}

	return make_shared<vulkan_graphics_pipeline>(
		shared_from_this(),
		static_pointer_cast<vulkan_pipeline_layout>(layout),
		move(vk_shaders),
		static_pointer_cast<vulkan_subpass>(subpass),
		move(vertex_bindings),
		move(vertex_attributes),
		topology,
		polygon_mode,
		flags
	);
}

shared_ptr<vulkan_semaphore> vulkan_context::create_semaphore() {
	return make_shared<vulkan_semaphore>(shared_from_this());
}

}