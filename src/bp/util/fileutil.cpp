#include <bp/util/fileutil.hpp>
#include <fstream>
#include <algorithm>

using namespace std;

namespace bp {

#if defined(WIN32) || defined(_WIN32) || defined(__WIN32) && !defined(__CYGWIN__)
const char path_separator = '\\';
#else
const char path_separator = '/';
#endif

static const string& ensure_path_format(string& path) {
#if defined(WIN32) || defined(_WIN32) || defined(__WIN32) && !defined(__CYGWIN__)
	replace(path.begin(), path.end(), '/', '\\');
#endif
	return path;
}

const string& native_path(string& path) {
	return ensure_path_format(path);
}

string native_path(string path) {
	ensure_path_format(path);
	return path;
}

string base_path(const string& path) {
	size_t pos = path.find_last_of(path_separator);
	string base = path.substr(0, pos);
	return base.empty() ? "." : base;
}

string file_name(const string& path) {
	size_t pos = path.find_last_of(path_separator);
	return path.substr(pos + 1);
}

vector<uint8_t> load_binary_file(const string& path) {
	ifstream file(path, ios::ate | ios::binary);

	if (!file.is_open()) {
		throw runtime_error("Failed to open '" + path + "'.");
	}

	size_t size = (size_t) file.tellg();
	vector<uint8_t> buffer(size);

	file.seekg(0);
	file.read(reinterpret_cast<char*>(buffer.data()), size);

	file.close();

	return buffer;
}

}